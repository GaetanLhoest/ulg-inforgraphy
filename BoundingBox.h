// Gray - A simple ray tracing program
// Copyright (C) 2008-2014 Eric Bechet
//
// See the COPYING file for contributions and license information.
// Please report all bugs and problems to <bechet@cadxfem.org>.
//

/**
 * Bounding Box :
 *
 *          4-----5
 *         /|    /|
 *        7-----6 |
 *        | |   | |
 *        | 3---|-2
 *        |/    |/
 *        0-----1
 */
#ifndef BOUNDINGBOX_H_
#define BOUNDINGBOX_H_

#include "models.h"
#include "meshmodel.h"
#include "modelsList.h"
#include "ray.h"
#include <vector>

class Face
{
public:
  std::vector<Point> points;
  Point min;
  Point max;

  Face ( std::vector<Point> pointsValue, Point minValue, Point maxValue )
    : points ( pointsValue ), min ( minValue ),max ( maxValue ) {}
};

class BoundingBox
{
public:
  BoundingBox ( Gmodel* );
  BoundingBox ( BoundingBox*, BoundingBox* );
  virtual ~BoundingBox() {}

  void sphereBB ( Gsphere* );
  void discBB ( Gdisc* );
  void meshModelBB ( Gmeshmodel* );

  Gmodel* getModel()
  {
    return model;
  }
  std::vector<Point> getCornerPoints()
  {
    return cornerPoints;
  }
  std::vector<Face> getFaces()
  {
    return faces;
  }
  Point getBBoxCenter()
  {
    return center;
  }

  void setModel ( Gmodel* newModel )
  {
    model = newModel;
  }
  void setCornerPoints ( std::vector<Point> newCornerPoints )
  {
    cornerPoints = newCornerPoints;
  }
  void setFaces ( std::vector<Face> newFaces )
  {
    faces = newFaces;
  }
  void setBBoxCenter ( Point newCenter )
  {
    center = newCenter;
  }

  bool intersect ( Ray* );

private:
  Gmodel* model;
  std::vector<Point> cornerPoints;
  std::vector<Face> faces;
  Point center;

  double getMinCoordinateX ( std::vector<Point> );
  double getMinCoordinateY ( std::vector<Point> );
  double getMinCoordinateZ ( std::vector<Point> );

  double getMaxCoordinateX ( std::vector<Point> );
  double getMaxCoordinateY ( std::vector<Point> );
  double getMaxCoordinateZ ( std::vector<Point> );

  bool intersectionX ( Face, Ray* );
  bool intersectionY ( Face, Ray* );
  bool intersectionZ ( Face, Ray* );

  void initFaces();
};

#endif /* BOUNDINGBOX_H_ */
// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
