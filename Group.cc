//
//  Group.cpp
//  GRAY
//
//  Created by Sébastien Rigaux on 15/04/14.
//
//

#include "Group.h"


unsigned int Group::groupIndex = 0;


string Group::getDefaultName() const {
    
    std::stringstream sstm;
    sstm << "Group" <<  groupIndex ;
    return sstm.str();
}

void Group::addSubGroup(const Group &group) {
    subGroupsNames.insert(group.name);
}


void Group::apply(const Gscene &scene, double fraction, Transform transform)
{
    Point p = transformOrigin;
    
    Transform t =
        transform
        .linearTransform(fraction)
        .atOrigin(p.X, p.Y, p.Z);
    
    apply(scene, t);
}
