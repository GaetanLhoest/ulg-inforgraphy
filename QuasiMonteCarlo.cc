// Gray - A simple ray tracing program
// Copyright (C) 2008-2014 Eric Bechet
//
// See the COPYING file for contributions and license information.
// Please report all bugs and problems to <bechet@cadxfem.org>.
//

#include "QuasiMonteCarlo.h"
#include <cstdlib>

float vandercorput ( unsigned int base, unsigned int number )
{
  double base_2 = base;
  double result = 0;
  double gardien = number*base;

  do
  {
    unsigned int quotient = number / base;
    result += ( number - quotient*base ) / base_2;
    number = quotient;
    base_2 *= base;
  }
  while ( base_2 <= gardien );

  return result;
}

void VdcRandomizer::setSeed ( unsigned int seed )
{
  state = seed;
}

float VdcRandomizer::getNext ( unsigned int base )
{
  #pragma omp critical
  {
    state++;
  }
//  return rand()*1.0/RAND_MAX;
  return vandercorput ( base,state );
}


// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
