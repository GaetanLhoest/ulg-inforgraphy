// Gray - A simple ray tracing program
// Copyright (C) 2008-2014 Eric Bechet
//
// See the COPYING file for contributions and license information.
// Please report all bugs and problems to <bechet@cadxfem.org>.
//

#ifndef QUASIMONTECARLO_H
#define QUASIMONTECARLO_H

#include <time.h>

float vandercorput ( unsigned int base, unsigned int number );

class VdcRandomizer
{
public :
  VdcRandomizer ( unsigned int seed = time ( 0 ) ) : state ( seed ) {};
  void setSeed ( unsigned int seed );
  float getNext ( unsigned int base );
private :
  unsigned int state;
};

#endif // QUASIMONTECARLO_H

// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
