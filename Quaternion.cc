//
//  Quaternion.cpp
//  GRAY
//
//  Created by Sébastien Rigaux on 25/03/14.
//
//

#include "Quaternion.h"

Quaternion::Quaternion()
{
    matrix = (QuaternionMatrix) {
        0, 0, 0, 0
    };
}

Quaternion::Quaternion(const Transform& transform)
{
    TransformMatrix m = transform.getMatrix();
    
    QuaternionMatrix q;
    
    if (m.a + m.e + m.i > 0)
    {
        float t = m.a + m.e + m.i + 1.;
        float s = 0.5/sqrt(t);
        
        q.w = s*t;
        q.z = (m.b - m.d)*s;
        q.y = (m.g - m.c)*s;
        q.x = (m.f - m.h)*s;
    }
    else if (m.a > m.e && m.a > m.i)
    {
        float t = m.a - m.e - m.i + 1.;
        float s = 0.5/sqrt(t);
        
        q.x = s*t;
        q.y = (m.b + m.d)*s;
        q.z = (m.g + m.c)*s;
        q.w = (m.f - m.h)*s;
    }
    else if (m.e > m.i)
    {
        float t = -m.a + m.e - m.i + 1.;
        float s = 0.5/sqrt(t);
        
        q.y = s*t;
        q.x = (m.b + m.d)*s;
        q.w = (m.g - m.c)*s;
        q.z = (m.f + m.h)*s;
    }
    else
    {
        float t = -m.a - m.e + m.i + 1.;
        float s = 0.5/sqrt(t);
        
        q.z = s*t;
        q.w = (m.b - m.d)*s;
        q.x = (m.g + m.c)*s;
        q.y = (m.f + m.h)*s;
    }
    
    matrix = q;
}

Transform Quaternion::toTransform()
{
    TransformMatrix m;
    QuaternionMatrix q = this->matrix;
    
    float x=q.x, y=q.y, z=q.z, w=q.w;
    
    m.a = 1 - 2*y*y - 2*z*z;
    m.b = 2*x*y + 2*w*z;
    m.c = 2*x*z - 2*w*y;
    m.tx = 0;
    
    m.d = 2*x*y - 2*w*z;
    m.e = 1 - 2*x*x - 2*z*z;
    m.f = 2*y*z + 2*w*x;
    m.ty = 0;
    
    m.g = 2*x*z + 2*w*y;
    m.h = 2*y*z - 2*w*x;
    m.i = 1 - 2*x*x - 2*y*y;
    m.tz = 0;
    
    m.p = 0;
    m.q = 0;
    m.r = 0;
    m.s = 1;
    
    return Transform(m);
}

Quaternion Quaternion::linearInterpolation(const Quaternion &qa, const Quaternion &qb, double fraction)
{

	QuaternionMatrix m;
    QuaternionMatrix a = qa.matrix;
    QuaternionMatrix b = qb.matrix;
    
    if (!memcmp(&a, &b, sizeof(a)))
        return qa;
    
    if (fraction == 0.)
        return qa;
    
    if (fraction == 1.)
        return qb;
    
    
    float dotproduct = a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w;
	float theta, st, sut, sout, coeff1, coeff2;
    
	theta = acos(dotproduct);
    if (theta == 0.0)
    {
        /* shouldn't happen, since we already checked for equality of
         inbound quaternions */
        /* if we didn't make this check, we'd get a lot of NaNs. */
        return qa;
    }
    
	if (theta < 0.0)
        theta = -theta;
	
	st = sin(theta);
	sut = sin(fraction * theta);
	sout = sin( (1-fraction) * theta);
	coeff1 = sout / st;
	coeff2 = sut / st;
    
	m.x = coeff1*a.x + coeff2*b.x;
	m.y = coeff1*a.y + coeff2*b.y;
	m.z = coeff1*a.z + coeff2*b.z;
	m.w = coeff1*a.w + coeff2*b.w;
    
    // normalize
    float qrLen = sqrt(m.x * m.x + m.y * m.y + m.z * m.z + m.w * m.w);
    
    m.x /= qrLen;
    m.y /= qrLen;
    m.z /= qrLen;
    m.w /= qrLen;
    
    return Quaternion(m);
}

Quaternion Quaternion::normalize() const
{
    QuaternionMatrix m = this->matrix;
    
    double len = sqrt(m.x * m.x +
                      m.y * m.y +
                      m.z * m.z +
                      m.w * m.w);
    
    m.x /= len;
    m.y /= len;
    m.z /= len;
    m.w /= len;
    
    return Quaternion(m);
}

