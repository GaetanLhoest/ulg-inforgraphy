//
//  Quaternion.h
//  GRAY
//
//  Created by Sébastien Rigaux on 25/03/14.
//
//

#ifndef __GRAY__Quaternion__
#define __GRAY__Quaternion__

#include <iostream>
#include "Transform.h"

class Transform;

typedef struct
{
    double x, y, z, w;
} QuaternionMatrix;

/**
 * Repr�sente un quaternion
 *
 * http://fr.wikipedia.org/wiki/Quaternion
 *
 */
class Quaternion
{
private:
	/**
	 * Retient la matrice du quaternion
	 */
    QuaternionMatrix matrix;
    
public:
    
    /**
     * Initialise un Quaternion (0,0,0,0)
     */
    Quaternion();

    /**
     * Initialise un Quaternion � partir d'un QuaternionMatrix
     */
    Quaternion(const QuaternionMatrix& matrix) : matrix(matrix) {}

    /**
     * Initialise un Quaternion � partir d'une matrice de transformation
     */
    Quaternion(const Transform& transform);

    /**
     * Initialise une copie du Quaternion quaternion
     */
    Quaternion(const Quaternion& quaternion) : matrix(quaternion.matrix) {}
    
    /**
     * Retourne la matrice repr�sentant le quaternion
     */
    QuaternionMatrix getMatrix() { return matrix; }
    
    /**
     * Retourne une Transformation correspondante � partir du quaternion
     */
    Transform toTransform();
    
    /**
     * Normalise le quaternion
     */
    Quaternion normalize() const;
    
    /**
     * Retourne un quaternion interpolant lin�airement
     * les quaternion 'a' et 'b' en fonction du pourcentage 'fraction'
     */
    static Quaternion linearInterpolation(const Quaternion& a,
                                          const Quaternion& b,
                                          double fraction);
};

#endif /* defined(__GRAY__Quaternion__) */
