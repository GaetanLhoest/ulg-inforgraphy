// Gray - A simple ray tracing program
// Copyright (C) 2008-2014 Eric Bechet
//
// See the COPYING file for contributions and license information.
// Please report all bugs and problems to <bechet@cadxfem.org>.
//
// Contributed by E. Balis, S. Hannay


#include <string>
#include <iostream>
#include <FL/Fl_BMP_Image.H>
#include <FL/Fl_PNG_Image.H>
#include "Texture.h"

Texture::Texture ( const std::string& path )
{
  image = 0;

  int length = path.length();
  std::string extension = path.substr ( length - 4, length );

  if ( extension == ".bmp" )
    image = new Fl_BMP_Image ( path.c_str() );
  else if ( extension == ".png" )
    image = new Fl_PNG_Image ( path.c_str() );
  else
    std::cout<<"Unrecognized format "<<path<<std::endl;

  //Check if the file is loaded
  if ( image == 0 || image->alloc_array == 0 )
  {
    std::cout<<"Could not load "<<path<<std::endl;

    delete image;
    image = 0;
  }
  else
    std::cout<<"Loaded "<<path<<" with "<<image->d() <<" channels"<<std::endl;
}

Texture::~Texture()
{
  if ( image != 0 )
    delete image;

  image = 0;
}

const uchar* Texture::getPixels() const
{
  if ( image != 0 )
    return image->array;

  return 0;
}

unsigned int Texture::getWidth() const
{
  if ( image != 0 )
    return image->w();

  return 0;
}

unsigned int Texture::getHeight() const
{
  if ( image != 0 )
    return image->h();

  return 0;
}

unsigned int Texture::getChannelNumber() const
{
  if ( image != 0 )
    return image->d();

  return 0;
}
// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
