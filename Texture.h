// Gray - A simple ray tracing program
// Copyright (C) 2008-2014 Eric Bechet
//
// See the COPYING file for contributions and license information.
// Please report all bugs and problems to <bechet@cadxfem.org>.
//
// Contributed by E. Balis, S. Hannay

#ifndef TEXTURE_H
#define TEXTURE_H

#include <FL/fl_types.h>

class Fl_RGB_Image;

class Texture
{
public:
  Texture ( const std::string& path );
  ~Texture();
  const uchar* getPixels() const;
  unsigned int getWidth() const;
  unsigned int getHeight() const;
  unsigned int getChannelNumber() const;

private:
  Fl_RGB_Image* image;
};

#endif
// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
