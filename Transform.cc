//
//  Transformation.cpp
//  GRAY
//
//  Created by Sébastien Rigaux on 9/03/14.
//
//

#include "Transform.h"

/**
 * Retourne le carr� du nombre 'x'
 */
#define E2(x) ((x)*(x))

const Transform Transform::identity = Transform();

Transform::Transform() {
    
    this->matrix = (TransformMatrix) {
        1, 0, 0, 0,
        0, 1, 0, 0,
        0, 0, 1, 0,
        0, 0, 0, 1
    };
}

bool Transform::isIdentity() const {
    return this->equalToTransform(Transform::identity);
}

bool Transform::equalToTransform(Transform const &t) const {
    
    TransformMatrix m1 = t.matrix;
    TransformMatrix m2 = this->matrix;
    
    return memcmp(&m1, &m2, sizeof(TransformMatrix));
}


Transform Transform::newTranslation(double tx, double ty, double tz)
{
    Transform t;
    
    t.matrix = (TransformMatrix) {
        1,  0,  0,  tx,
        0,  1,  0,  ty,
        0,  0,  1,  tz,
        0,  0,  0,  1
    };

    return t;
}

Transform Transform::newScale(double sx, double sy, double sz)
{
    Transform t;
    
    t.matrix = (TransformMatrix) {
        sx, 0,  0,  0,
        0,  sy, 0,  0,
        0,  0,  sz, 0,
        0,  0,  0,  1
    };
    
    return t;
}

Transform Transform::newRotation(double radians, double x, double y, double z)
{
    /* Zero length vector returns identity */
    if (x == 0.0 && y == 0.0 && z == 0.0)
        return identity;
    
    
    /* Precalculate sin and cos */
    double s = sin(radians);
    double c = cos(radians);
    
    /* Normalize vector */
    double len = sqrt(x * x + y * y + z * z);
    x /= len; y /= len; z /= len;
    
    /* Fill the return value */
    Transform returnValue;
    
    returnValue.matrix = (TransformMatrix){
        c + (1-c) * x*x,    (1-c) * x*y + s*z,  (1-c) * x*z - s*y,  0,
        (1-c) * y*x - s*z,  c + (1-c) * y*y,    (1-c) * y*z + s*x,  0,
        (1-c) * z*x + s*y,  (1-c) * y*z - s*x,  c + (1-c) * z*z,    0,
        0,                  0,                  0,                  1
    };
    
    return returnValue;
}

/* idea for a GS extension: rotation matrices for rotation around x, y and z.
 somewhat simpler.
 - x: e=c, f=s,  h=-s, i=c
 - y: a=c, c=-s, g=c,  i=c
 - z: a=c, b=s,  d=-s, e=c
 */

Transform Transform::translate(double tx, double ty, double tz) const
{
    return concat(newTranslation(tx, ty, tz));
}

Transform Transform::scale(double sx, double sy, double sz) const
{
    return concat(newScale(sx, sy, sz));
}

Transform Transform::rotate(double radians, double x, double y, double z) const
{
    return concat(newRotation(radians, x, y, z));
}

Transform Transform::concat(const Transform &t) const
{
    // multiplication
    Transform result;
    
    TransformMatrix m = this->matrix;
    TransformMatrix n = t.matrix;
    TransformMatrix r;
    
    r.a  = ( m.a  * n.a) + ( m.d  * n.b) + ( m.g  * n.c) + ( m.p * n.tx);
    r.b  = ( m.b  * n.a) + ( m.e  * n.b) + ( m.h  * n.c) + ( m.q * n.tx);
    r.c  = ( m.c  * n.a) + ( m.f  * n.b) + ( m.i  * n.c) + ( m.r * n.tx);
    r.tx = ( m.tx * n.a) + ( m.ty * n.b) + ( m.tz * n.c) + ( m.s * n.tx);
    
    r.d  = ( m.a  * n.d) + ( m.d  * n.e) + ( m.g  * n.f) + ( m.p * n.ty);
    r.e  = ( m.b  * n.d) + ( m.e  * n.e) + ( m.h  * n.f) + ( m.q * n.ty);
    r.f  = ( m.c  * n.d) + ( m.f  * n.e) + ( m.i  * n.f) + ( m.r * n.ty);
    r.ty = ( m.tx * n.d) + ( m.ty * n.e) + ( m.tz * n.f) + ( m.s * n.ty);
    
    r.g  = ( m.a  * n.g) + ( m.d  * n.h) + ( m.g  * n.i) + ( m.p * n.tz);
    r.h  = ( m.b  * n.g) + ( m.e  * n.h) + ( m.h  * n.i) + ( m.q * n.tz);
    r.i  = ( m.c  * n.g) + ( m.f  * n.h) + ( m.i  * n.i) + ( m.r * n.tz);
    r.tz = ( m.tx * n.g) + ( m.ty * n.h) + ( m.tz * n.i) + ( m.s * n.tz);
    
    r.p = ( m.a  * n.p)  + ( m.d  * n.q) + ( m.g  * n.r) + ( m.p * n.s);
    r.q = ( m.b  * n.p)  + ( m.e  * n.q) + ( m.h  * n.r) + ( m.q * n.s);
    r.r = ( m.c  * n.p)  + ( m.f  * n.q) + ( m.i  * n.r) + ( m.r * n.s);
    r.s = ( m.tx * n.p)  + ( m.ty * n.q) + ( m.tz * n.r) + ( m.s * n.s);
    
    result.matrix = r;
    
    return result;
}

Transform Transform::atOrigin(double x, double y, double z) const {
    return
        newTranslation(-x, -y, -z).concat(*this).translate(x,y,z);
}

Transform Transform::linearTransform(double fraction)
{
    return Transform::linearTransform(Transform::identity,
                                      *this,
                                      fraction);
}


double Transform::determinant2x2(double a, double b,
                                 double d, double e)
{
    return a * e - b * d;
}


double Transform::determinant3x3(double a, double b, double c,
                                 double d, double e, double f,
                                 double g, double h, double i)
{
    return a * determinant2x2(e, f, h, i )
         - d * determinant2x2(b, c, h, i )
         + g * determinant2x2(b, c, e, f );
}


double Transform::determinant4x4(const Transform &t)
{
    TransformMatrix m = t.matrix;
    
    return
      m.a * determinant3x3(m.e, m.f, m.ty, m.h, m.i, m.tz, m.q, m.r, m.s)
    - m.d * determinant3x3(m.b, m.c, m.tx, m.h, m.i, m.tz, m.q, m.r, m.s)
    + m.g * determinant3x3(m.b, m.c, m.tx, m.e, m.f, m.ty, m.q, m.r, m.s)
    - m.p * determinant3x3(m.b, m.c, m.tx, m.e, m.f, m.ty, m.h, m.i, m.tz);
}

Transform Transform::invert() const
{
    const double epsilon = 0.0001; /* TODO: which value should we use? */
    
    double determinant = determinant4x4(*this);
    
    if (fabs(determinant) > epsilon) {
        /* can be inverted */
        
        TransformMatrix m = this->matrix;
        TransformMatrix n;

        n.a  =   determinant3x3( m.e,  m.f,  m.ty,  m.h,  m.i,  m.tz,  m.q,  m.r,  m.s );
        n.b  = - determinant3x3( m.b,  m.c,  m.tx,  m.h,  m.i,  m.tz,  m.q,  m.r,  m.s );
        n.c  =   determinant3x3( m.b,  m.c,  m.tx,  m.e,  m.f,  m.ty,  m.q,  m.r,  m.s );
        n.tx = - determinant3x3( m.b,  m.c,  m.tx,  m.e,  m.f,  m.ty,  m.h,  m.i,  m.tz);
        
        n.d  = - determinant3x3( m.d,  m.f,  m.ty,  m.g,  m.i,  m.tz,  m.p,  m.r,  m.s );
        n.e  =   determinant3x3( m.a,  m.c,  m.tx,  m.g,  m.i,  m.tz,  m.p,  m.r,  m.s );
        n.f  = - determinant3x3( m.a,  m.c,  m.tx,  m.d,  m.f,  m.ty,  m.p,  m.r,  m.s );
        n.ty =   determinant3x3( m.a,  m.c,  m.tx,  m.d,  m.f,  m.ty,  m.g,  m.i,  m.tz);
        
        n.g  =   determinant3x3( m.d,  m.e,  m.ty,  m.g,  m.h,  m.tz,  m.p,  m.q,  m.s );
        n.h  = - determinant3x3( m.a,  m.b,  m.tx,  m.g,  m.h,  m.tz,  m.p,  m.q,  m.s );
        n.i  =   determinant3x3( m.a,  m.b,  m.tx,  m.d,  m.e,  m.ty,  m.p,  m.q,  m.s );
        n.tz = - determinant3x3( m.a,  m.b,  m.tx,  m.d,  m.e,  m.ty,  m.g,  m.h,  m.tz);
        
        n.p = - determinant3x3( m.d,  m.e,  m.f,  m.g,  m.h,  m.i,  m.p,  m.q,  m.r);
        n.q =   determinant3x3( m.a,  m.b,  m.c,  m.g,  m.h,  m.i,  m.p,  m.q,  m.r);
        n.r = - determinant3x3( m.a,  m.b,  m.c,  m.d,  m.e,  m.f,  m.p,  m.q,  m.r);
        n.s =   determinant3x3( m.a,  m.b,  m.c,  m.d,  m.e,  m.f,  m.g,  m.h,  m.i);
        
        Transform t;
        t.matrix = n;
        
        return t;
    }
    else
    {
        /* cannot be inverted */
        return *this;
    }
}

Transform Transform::transpose() const
{
    TransformMatrix m = this->matrix;
    TransformMatrix r;
    
    double *mF = (double *)&m;
    double *rF = (double *)&r;
    
    for (int i = 0; i < 16; i++)
    {
        int c = i % 4;
        int r = i / 4;
        int j = c * 4 + r;
        rF[j] = mF[i];
    }
    
    return Transform(r);
}

static float linearInterpolation(float from, float to, float fraction)
{
    return from + (to - from) * fraction;
}

Transform Transform::linearTransform(const Transform &tfrom, const Transform &tto, double fraction)
{
    
    if (fraction == 0.)
        return tfrom;
    
    if (fraction == 1.)
        return tto;
    
    TransformMatrix from = tfrom.matrix;
    TransformMatrix to = tto.matrix;
    
    double fromTX = from.tx, fromTY = from.ty, fromTZ = from.tz;
    double   toTX =   to.tx,   toTY =   to.ty,   toTZ =   to.tz;
    
    double valueTX = linearInterpolation(fromTX, toTX, fraction);
    double valueTY = linearInterpolation(fromTY, toTY, fraction);
    double valueTZ = linearInterpolation(fromTZ, toTZ, fraction);
    
    double fromSX = sqrt(E2(from.a) + E2(from.b) + E2(from.c));
    double fromSY = sqrt(E2(from.d) + E2(from.e) + E2(from.f));
    double fromSZ = sqrt(E2(from.g) + E2(from.h) + E2(from.i));
    
    double toSX = sqrt(E2(to.a) + E2(to.b) + E2(to.c));
    double toSY = sqrt(E2(to.d) + E2(to.e) + E2(to.f));
    double toSZ = sqrt(E2(to.g) + E2(to.h) + E2(to.i));
    
    double valueSX = linearInterpolation(fromSX, toSX, fraction);
    double valueSY = linearInterpolation(fromSY, toSY, fraction);
    double valueSZ = linearInterpolation(fromSZ, toSZ, fraction);
    
    /* rotation */
    Transform fromR((TransformMatrix) {
        from.a/fromSX,    from.b/fromSX,    from.c/fromSX,    0,
        from.d/fromSY,    from.e/fromSY,    from.f/fromSY,    0,
        from.g/fromSZ,    from.h/fromSZ,    from.i/fromSZ,    0,
        0,                0,                0,                1
    });
    
    Transform toR((TransformMatrix) {
        to.a/toSX,        to.b/toSX,        to.c/toSX,        0,
        to.d/toSY,        to.e/toSY,        to.f/toSY,        0,
        to.g/toSZ,        to.h/toSZ,        to.i/toSZ,        0,
        0,                0,                0,                1
    });
    
    Quaternion fromRQ = Quaternion(fromR).normalize();
    Quaternion toRQ   = Quaternion(toR).normalize();
    
    Quaternion rRq = Quaternion::linearInterpolation(fromRQ, toRQ, fraction);
    
    TransformMatrix m = rRq.toTransform().getMatrix();
    
    m.a *= valueSX;
    m.b *= valueSX;
    m.c *= valueSX;
    
    m.d *= valueSY;
    m.e *= valueSY;
    m.f *= valueSY;
    
    m.g *= valueSZ;
    m.h *= valueSZ;
    m.i *= valueSZ;
    
    m.tx = valueTX;
    m.ty = valueTY;
    m.tz = valueTZ;
    
    return Transform(m);
}

Point Transform::apply(const Point &p) const
{
    
    double x, y, z;
    double w = 1.0f;
    
    TransformMatrix m = this->matrix;
    
    x =  m.a * p.X +  m.b * p.Y +  m.c * p.Z +  m.tx * w;
    y =  m.d * p.X +  m.e * p.Y +  m.f * p.Z +  m.ty * w;
    z =  m.g * p.X +  m.h * p.Y +  m.i * p.Z +  m.tz * w;
    w =  m.p * p.X +  m.q * p.Y +  m.r * p.Z +  m.s  * w;
    
    Point resultPoint = Point(x/w, y/w, z/w);
    
    return resultPoint;
}

ostream & operator<<(ostream &out, const Transform &t)
{

    out << "Transform : [ \n";
    
    out.precision(3);
    
    out << std::fixed;
    
    TransformMatrix m = t.matrix;
    
    out << '\t' << m.a << '\t' << m.b << '\t' << m.c << '\t' << m.tx << '\n'
        << '\t' << m.d << '\t' << m.e << '\t' << m.f << '\t' << m.ty << '\n'
        << '\t' << m.g << '\t' << m.h << '\t' << m.i << '\t' << m.tz << '\n'
        << '\t' << m.p << '\t' << m.q << '\t' << m.r << '\t' << m.s  << '\n';
    
    
    out << "]\n\n";
    
    return out;
}
