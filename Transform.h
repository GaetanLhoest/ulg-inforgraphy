//
//  AffineTransformation.h
//  GRAY
//
//  Created by Sébastien Rigaux on 9/03/14.
//
//

#ifndef __GRAY__Transform__
#define __GRAY__Transform__

#include <iostream>
#include <math.h>
#include <stdio.h>
#include <string.h>

#include "Quaternion.h"
#include "ray.h"

using namespace std;

/**
 * Transforme des degr�s en radians
 */
inline double DegToRad(double x)
{
    return x / 180 * M_PI;
}

/**
 * Transforme des randians en degr�s
 */
inline double RadToDeg(double x)
{
    return x / M_PI * 180.0f;
}

/**
 * Matrice de transformation (4x4)
 */
typedef struct
{
    double a, b, c, tx,
           d, e, f, ty,
           g, h, i, tz,
           p, q, r, s;
    
} TransformMatrix;

/**
 * Repr�sente une transformation 3D
 * pouvant �tre appliqu� � des objets ou des points
 */
class Transform
{
    
private:
    
	/**
	 * La matrice de transformation
	 */
    TransformMatrix matrix;
    
    /**
     * Calcule le d�terminant d'une matrice 2x2
     */
    static double determinant2x2(double a, double b,
                                 double d, double e);
    
    /**
     * Calcule le d�terminant d'une matrice 3x3
     */
    static double determinant3x3(double a, double b, double c,
                                 double d, double e, double f,
                                 double g, double h, double i);
    
    /**
     * Calcule le d�terminant d'une matrice 4x4
     */
    static double determinant4x4(const Transform &t);
    
public:

    /**
     * Transformation identit�
     */
    static const Transform identity;
    
    /**
     * Initialise une transformation identit�
     */
    Transform();

    /**
     * Initialise une copie de la TransformMatrix matrix
     */
    Transform(TransformMatrix matrix) : matrix(matrix) {}
    
    /**
     * Cr�e une transformation correspondante � une translation
     * de tx en X, ty en Y et tz en z
     */
    static Transform newTranslation(double tx, double ty, double tz);

    /**
     * Cr�e une transformation correspondante � une mise � l'�chelle
     * de rapport s (dans les 3 dimensions)
     */
    static Transform newScale(double s) { return newScale(s,s,s); };

    /**
     * Cr�e une transformation correspondante � une mise � l'�chelle
     * de rapport sx en X, sy en Y et sz en Z
     */
    static Transform newScale(double sx, double sy, double sz);

    /**
     * Cr�e une transformation correspondante � une rotation
     * d'angle 'angle' autour du vecteur (x,y,z)
     *
     *	/!\ comportement ind�termin� si la norme du vecteur est nulle
     */
    static Transform newRotation(double angle, double x, double y, double z);
    
    /**
     * Retourne une Transformation correspondante � l'interpolation lin�aire entre
     * la transformation 'ta' et 'tb'
     * a un pourcentage 'fraction' de compl�tude
     *
     */
    static Transform linearTransform(const Transform& ta, const Transform& tb, double fraction);
    
    /**
     * Retourne la matrice de transformation associ�e
     */
    TransformMatrix getMatrix() const { return matrix; }
    
    /**
     * Retourne true si la matrice est la matrice identit�, false sinon
     */
    bool isIdentity() const;

    /**
     * Retourne true si la transformation est la m�me que 't', false sinon
     */
    bool equalToTransform(const Transform &t) const;
    
    /**
     * Retourne une transformation correspondante � celle actuelle � laquelle
     * on a appliqu� une transformation 't'
     */
    Transform concat(const Transform &t) const;

    /**
     * Retourne l'inverse la transformation actuelle
     */
    Transform invert() const;

    /**
     * Retourne une transformation correspondante � celle actuelle � laquelle
     * on a appliqu� une translation en tx d'axe X, ty d'axe Y et tz d'axe Z
     */
    Transform translate(double tx, double ty, double tz) const;

    /**
     * Retourne une transformation correspondante � celle actuelle � laquelle
     * on a appliqu� une mise � l'�chelle de rapport sx en X, sy en Y et sz en Z
     */
    Transform scale(double sx, double sy, double sz) const;

    /**
     * Retourne une transformation correspondante � celle actuelle � laquelle
     * on a appliqu� une mise � l'�chelle de rapport s
     */
    Transform scale(double s) const { return scale(s,s,s); }
    
    /**
     * Retourne une transformation correspondante � celle actuelle � laquelle
     * on a appliqu� rotation de 'radians' radians autour du vecteur (x, y, z)
     */
    Transform rotate(double radians, double x, double y, double z) const;

    /**
     * Retourne une transformation correspondante � la transpos�e de celle actuelle
     */
    Transform transpose() const;
    

    /**
     * Retourne une transformation correspondante � celle actuelle mais dont
     * le centre est le point (x,y,z)
     */
    Transform atOrigin(double x, double y, double z) const;
    
    /**
	 * Retourne une Transformation correspondante � l'interpolation lin�aire entre
	 * la transformation identit�e et l'actuelle
	 * a un pourcentage 'fraction' de compl�tude
	 */
    Transform linearTransform(double fraction);
    
    /**
     * Retourne un point correspondant au point 'point' auquel on a appliqu�
     * la transformation actuelle
     */
    Point apply(const Point &point) const;
    
    friend ostream & operator<<(ostream & out, const Transform &t);
};

// For debugging:
void TransformPrint(Transform t);

#endif /* defined(__GRAY__AffineTransformation__) */
