//
//  VertexGroup.h
//  GRAY
//
//  Created by Sébastien Rigaux on 15/04/14.
//
//

#ifndef __GRAY__VertexGroup__
#define __GRAY__VertexGroup__

#include "Group.h"

using namespace std;

typedef std::set<int> IntSet;

/**
 *
 * Classe permettant de regrouper des vertex au sein d'un Gmeshmodel
 * pour �tre anim�s ensemble.
 *
 * Voir Group.h pour commentaires non d�finis
 */
class VertexGroup : public Group {
    
protected:

	/**
	 * Retient le nom (identifiant) du meshmodel associ�
	 */
    string meshModelName;

    /**
     * Retient l'ensemble des indices des vertex du meshModel
     * concern�s par le groupe
     */
    IntSet vertexIndexes;
    
    string getDefaultName() const;
    
public:
    
    VertexGroup(const Gmeshmodel& meshModel):
        Group(getDefaultName(), Point(0,0,0)),
        meshModelName(meshModel.getName()),
        vertexIndexes()
        {}
    
    VertexGroup(const VertexGroup& g):
        Group(g),
        meshModelName(g.meshModelName),
        vertexIndexes(g.vertexIndexes)
        {}
    
    VertexGroup(Point origin, const Gmeshmodel& meshModel) :
        Group(getDefaultName(), origin),
        meshModelName(meshModel.getName()),
        vertexIndexes()
        {}
    
    VertexGroup(string name, Point origin, const Gmeshmodel& meshModel) :
        Group(name, origin),
        meshModelName(meshModel.getName()),
        vertexIndexes()
        {}
    
    Group* clone() const;
    
    /**
     * Ajoute un nouveau vertex (en donnant sont indice)
     */
    void addVertexIndex(int index);

    /**
     * Ajoute un ensemble de vertex (en donnant leurs indices)
     */
    void addVertexIndexes(int* indexes, int count);
    
    void addSubGroup(const VertexGroup& group) { Group::addSubGroup(group); }
    
    /**
     * Ajoute un �l�ment au groupe (indice de vertex ou VertexGroup)
     */
    void add(int index) { addVertexIndex(index); }
    void add(const VertexGroup& group) { Group::add(group); }
    
    /**
     * Retourne l'ensemble des indices des vertex du meshmodel
     * concern� par ce groupe
     */
    IntSet getVertexIndexes() const { return vertexIndexes; }
    
    void apply(const Gscene &scene, Transform t);
};


#endif /* defined(__GRAY__VertexGroup__) */
