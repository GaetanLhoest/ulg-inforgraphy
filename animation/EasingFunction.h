//
//  EasingFunction.h
//  GRAY
//
//  Created by Sébastien Rigaux on 4/04/14.
//
//

#ifndef __GRAY__EasingFunction__
#define __GRAY__EasingFunction__

#include <iostream>
#include <math.h>

typedef enum {
    EaseIn,
    EaseOut,
    EaseInOut,
} Ease;

class EasingFunction {
    
protected:
    Ease easing;
    
    virtual float easeIn(float fraction) const = 0;
    virtual float easeOut(float fraction) const = 0;
    virtual float easeInOut(float fraction) const = 0;
    
public:
    
    EasingFunction() : easing(EaseInOut) {}
    EasingFunction(Ease easing) : easing(easing) {}
    
    float ease(float current,
               float start,
               float duration) const;
};

#define EasingFunctionClassDefinition(name, ease) \
class name##EasingFunction : public EasingFunction { \
 \
float easeIn(float fraction) const; \
float easeOut(float fraction) const; \
float easeInOut(float fraction) const; \
 \
public: \
 \
name##EasingFunction() : EasingFunction(ease) {} \
 \
}

EasingFunctionClassDefinition(Linear, EaseIn);
EasingFunctionClassDefinition(Quadratic, EaseInOut);
EasingFunctionClassDefinition(Cubic, EaseInOut);
EasingFunctionClassDefinition(Quartic, EaseInOut);
EasingFunctionClassDefinition(Quintic, EaseInOut);
EasingFunctionClassDefinition(Sine, EaseInOut);
EasingFunctionClassDefinition(Circular, EaseInOut);
EasingFunctionClassDefinition(Exponential, EaseInOut);
EasingFunctionClassDefinition(Elastic, EaseOut);
EasingFunctionClassDefinition(Back, EaseOut);
EasingFunctionClassDefinition(Bounce, EaseOut);



#endif /* defined(__GRAY__EasingFunction__) */
