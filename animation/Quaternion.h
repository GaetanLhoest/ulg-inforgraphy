//
//  Quaternion.h
//  GRAY
//
//  Created by Sébastien Rigaux on 25/03/14.
//
//

#ifndef __GRAY__Quaternion__
#define __GRAY__Quaternion__

#include <iostream>
#include "../transform/Transform.h"

class Transform;

typedef struct
{
    double x, y, z, w;
} QuaternionMatrix;

class Quaternion
{
private:
    QuaternionMatrix matrix;
    
public:
    
    Quaternion();
    Quaternion(const QuaternionMatrix& matrix) : matrix(matrix) {}
    Quaternion(const Transform& transform);
    Quaternion(const Quaternion& quaternion) : matrix(quaternion.matrix) {}
    
    QuaternionMatrix getMatrix() { return matrix; }
    
    Transform toTransform();
    
    Quaternion normalize() const;
    
    static Quaternion linearInterpolation(const Quaternion& a,
                                          const Quaternion& b,
                                          double fraction);
};

#endif /* defined(__GRAY__Quaternion__) */
