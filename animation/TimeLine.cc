//
//  TimeLine.cc
//  GRAY
//
//  Created by Sébastien Rigaux on 4/04/14.
//
//

#include "TimeLine.h"

void TimeLine::addKeyFrame(KeyFrame* keyFrame)
{
    unsigned int atFrame = keyFrame->getStart();
    
    KeyFrameList& list = frames[atFrame];
    
    list.push_back(keyFrame);
}

Gscene* TimeLine::getSceneAtFrame(unsigned int frame)
{
    //copy the scene
    
    Gscene* result = new Gscene(scene);
    
    KeyFrameList keys;
    
    unsigned int i = 0;
    
    //Add all keys
    for (FrameList::iterator it = frames.begin() ;
         it != frames.end() && i <= frame;
         ++it, i++) {
        
        for (KeyFrameList::iterator kit = (*it).begin();
             kit != (*it).end();
             ++kit) {
            
            KeyFrame* kf = (*kit);
            KeyFrame* cp = kf;
            
            keys.push_back(cp);
            
        }
    }
    
    for (KeyFrameList::iterator kit = keys.begin();
         kit != keys.end(); ++ kit) {
    
        KeyFrame* kf = (*kit);
        
        kf->apply(*result, frame);
    }
    
    return result;
}

Gscene* TimeLine::getCurrentScene()
{
    if (currentScene == NULL)
        currentScene = getSceneAtFrame(currentFrame);
    
    return currentScene;
}

int TimeLine::goToNextFrame()
{
    return this->goToFrameOffset(1);
}

int TimeLine::goToPreviousFrame()
{
    return this->goToFrameOffset(-1);
}

int TimeLine::goToFrameOffset(int offset)
{
    unsigned int toFrame = currentFrame + offset;
    
    return goToFrame(toFrame);
}

int TimeLine::goToFrame(unsigned int frame)
{
    if (frame == currentFrame)
        return frame;
    
    if (frame > framesCount)
        return -1;
    
    currentFrame = frame;
    
    if (currentScene != NULL)
        delete currentScene;
    
    currentScene = getSceneAtFrame(currentFrame);
    
    return currentFrame;
}