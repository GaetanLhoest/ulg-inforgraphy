//
//  TimeLine.h
//  GRAY
//
//  Created by Sébastien Rigaux on 4/04/14.
//
//

#ifndef __GRAY__TimeLine__
#define __GRAY__TimeLine__

#include <iostream>
#include "../scene.h"
#include "KeyFrame.h"

typedef std::list<KeyFrame*> KeyFrameList;
typedef std::vector<KeyFrameList> FrameList;

class TimeLine {
private:
    unsigned int framesCount;
    Gscene& scene;
    FrameList frames;
    
    unsigned int currentFrame;
    Gscene* currentScene;
    
public:
    
    TimeLine(unsigned int framesCount, Gscene& scene) :
        framesCount(framesCount),
        scene(scene),
        frames(framesCount),
        currentFrame(0),
        currentScene(NULL)
    {}
    
    unsigned int getCurrentFrame() { return currentFrame; }
    unsigned int getFramesCount() { return framesCount; }
    Gscene& getScene() { return scene; }
    Gscene* getCurrentScene();
    
    void addKeyFrame(KeyFrame* keyFrame);
    
    Gscene* getSceneAtFrame(unsigned int frame);
    
    int goToNextFrame();
    int goToPreviousFrame();
    int goToFrameOffset(int offset);
    int goToFrame(unsigned int frame);
};

#endif /* defined(__GRAY__TimeLine__) */
