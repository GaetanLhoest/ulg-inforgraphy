//
//  AutoReverseKeyFrame.h
//  GRAY
//
//  Created by Sébastien Rigaux on 8/04/14.
//
//

#ifndef __GRAY__AutoReverseKeyFrame__
#define __GRAY__AutoReverseKeyFrame__

#include <iostream>
#include "SimpleKeyFrame.h"

class AutoReverseKeyFrame : public KeyFrame {

protected:
    SimpleKeyFrame normalKeyFrame;
    SimpleKeyFrame reverseKeyFrame;
    
    SimpleKeyFrame* getSimpleKeyFrame(unsigned int frame);
    
public:
    
    AutoReverseKeyFrame(unsigned int start,
                        unsigned int duration,
                        const ModelGroup& group,
                        Transform transform,
                        const EasingFunction& easing):
        normalKeyFrame(start, duration, group, transform, easing),
        reverseKeyFrame(start + duration, duration, group, transform, easing)
        {}
    
    AutoReverseKeyFrame(const AutoReverseKeyFrame& k):
        normalKeyFrame(k.normalKeyFrame),
        reverseKeyFrame(k.reverseKeyFrame)
        {}
    
    KeyFrame* clone() { return new AutoReverseKeyFrame(*this); }
    
    unsigned int getStart() const {
        return normalKeyFrame.getStart();
    }
    
    unsigned int getDuration() const {
        return reverseKeyFrame.getEnd() - normalKeyFrame.getStart();
    }
    
    unsigned int getEnd() const {
        return reverseKeyFrame.getEnd();
    }
    
    string getGroup() const { return normalKeyFrame.getGroupName(); }
    
    double getInterpolationFractionForFrame(unsigned int frame);
    
    Transform getTransform() const { return normalKeyFrame.getTransform(); }
    
    void apply(const Gscene& scene, int frame);
    
};

#endif /* defined(__GRAY__AutoReverseKeyFrame__) */
