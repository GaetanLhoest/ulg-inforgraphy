//
//  SimpleKeyFrame.cpp
//  GRAY
//
//  Created by Sébastien Rigaux on 8/04/14.
//
//

#include "SimpleKeyFrame.h"
#include <assert.h>

double SimpleKeyFrame::getInterpolationFractionForFrame(unsigned int frame)
{
    if (frame <= start)
        return 0.;

    if (frame >= getEnd())
        return 1.;

    double fraction = (frame - start) / (double)duration;

    fraction = easing.ease(frame, start, duration);
    return fraction;
}

void SimpleKeyFrame::apply(const Gscene &scene, int frame)
{
    double fraction = getInterpolationFractionForFrame(frame);

    Group * modelGroup = scene.getGroup(groupName);

    assert(modelGroup != NULL);
    
    modelGroup->apply(scene, fraction, transform);
}















