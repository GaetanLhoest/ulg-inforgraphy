// Gray - A simple ray tracing program
// Copyright (C) 2008-2014 Eric Bechet
//
// See the COPYING file for contributions and license information.
// Please report all bugs and problems to <bechet@cadxfem.org>.
//

#include "camera.h"

double Camera::getLens()
{
  return normalLens;
}
void Camera::setLens ( double lens )
{
  normalLens = lens;
}

Point Camera::getDirection()
{
  return direction;
}
void Camera::setDirection ( Point dir )
{
  direction = dir;
}

Point Camera::getUp()
{
  return up;
}
void Camera::setUp ( Point u )
{
  up = u;
}

double Camera::getFocal()
{
  return focalDistance;
}
void Camera::setFocal ( double focal )
{
  focalDistance = focal;
}

double Camera::getFocusDist()
{
  return focusDistance;
}
void Camera::setFocusDist ( double focus )
{
  focusDistance = focus;
}

double Camera::getDiaph()
{
  return diaphragmAperture;
}
void Camera::setDiaph ( double diaph )
{
  diaphragmAperture = diaph;
}

double Camera::getNearclip()
{
  return nearclip;
}
void Camera::setNearclip ( double n )
{
  nearclip = n;
}

double Camera::getFarclip()
{
  return farclip;
}
void Camera::setFarclip ( double f )
{
  farclip = f;
}
// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
