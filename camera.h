// Gray - A simple ray tracing program
// Copyright (C) 2008-2014 Eric Bechet
//
// See the COPYING file for contributions and license information.
// Please report all bugs and problems to <bechet@cadxfem.org>.
//

#ifndef __CAMERA_H_
#define __CAMERA_H_

#include "ray.h" //Point

/* Default values for the parameters of the camera */
#define DEFAULT_LENS 4.326661531 //Value for 24x36
#define DEFAULT_DIR 0.,0.,0.
#define DEFAULT_UP 0.,1.,0.
#define DEFAULT_FOCAL 2.8 //28 mm
#define DEFAULT_FOCUS 30
#define DEFAULT_DIAPH 22
#define DEFAULT_NEARCLIP 1.
#define DEFAULT_FARCLIP 4000.

class Camera
{
public:
  /* Constructors */
  Camera()
  {
    normalLens = DEFAULT_LENS;
    direction = Point ( DEFAULT_DIR );
    up = Point ( DEFAULT_UP );
    focalDistance = DEFAULT_FOCAL;
    focusDistance = DEFAULT_FOCUS;
    diaphragmAperture = DEFAULT_DIAPH;
    nearclip = DEFAULT_NEARCLIP;
    farclip = DEFAULT_FARCLIP;
  }

  /* Accessors and modifiers */
  double getLens();
  void setLens ( double lens );

  Point getDirection();
  void setDirection ( Point dir );

  Point getUp();
  void setUp ( Point u );

  double getFocal();
  void setFocal ( double focal );

  double getFocusDist();
  void setFocusDist ( double focus );

  double getDiaph();
  void setDiaph ( double diaph );

  double getNearclip();
  void setNearclip ( double n );

  double getFarclip();
  void setFarclip ( double f );

private:
  double normalLens;
  Point direction; //Point to aim
  Point up; //Direction of upper side of cam
  double focalDistance;
  double focusDistance;
  double diaphragmAperture;
  double nearclip;
  double farclip;
};


#endif //__CAMERA_H_
// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
