// Gray - A simple ray tracing program
// Copyright (C) 2008-2014 Eric Bechet
//
// See the COPYING file for contributions and license information.
// Please report all bugs and problems to <bechet@cadxfem.org>.
//

#ifndef __FRAMEBUFFER_H_
#define __FRAMEBUFFER_H_

#include <ostream>
#include "ray.h"
#include "TimeLine.h"

class MyGlWindow;

class framebuffer
{
public:
  framebuffer (TimeLine* tl) : timeLine(tl), empty(true) {}

  virtual ~framebuffer() {}
  virtual Color& operator() ( int i,int j ) =0;
  virtual void makeimage ( void );
  virtual size_t getwidth() =0;
  virtual size_t getheight() =0;
  virtual void resize ( size_t w_,size_t h_ ) =0;
  virtual void clear ( void ) =0;
  virtual bool ok()
  {
    return !empty;
  }
  virtual void get_coords ( float i,float j,Ray &r ) =0;
  virtual TimeLine * getTimeLine ( void )
  {
    return timeLine;
  }
private:
  TimeLine * timeLine;
protected :
  bool empty;
};

class basic_framebuffer : public framebuffer
{
public:
  basic_framebuffer ( size_t w_,size_t h_, TimeLine * timeLine);
  virtual ~basic_framebuffer()
  {
    delete[] buffer;
  }
  virtual Color& operator() ( int i,int j )
  {
    return buffer[i+j*w ];
  }
  virtual float * getbuffer ( void )
  {
    return ( float * ) buffer;
  }
  virtual size_t getwidth()
  {
    return w;
  }
  virtual size_t getheight()
  {
    return h;
  }
  virtual void resize ( size_t w_,size_t h_ );
  virtual void clear ( void );
  virtual void get_coords ( float i,float j,Ray &r ) =0;
  virtual void save_image ( std::ostream &file );
  virtual void save_image_png(const char* filename);
private :
  Color * buffer;
  size_t w;
  size_t h;
  size_t mem;
};

class image_framebuffer : public basic_framebuffer
{
public:
    image_framebuffer(size_t w,size_t h, TimeLine * timeLine):
        basic_framebuffer(w, h, timeLine)
        {}
    
    void get_coods(float i, float j, Ray& ray);
};


#endif //__FRAMEBUFFER_H_

// kate: indent-mode cstyle; indent-width 2; replace-tabs on;
