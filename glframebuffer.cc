// Gray - A simple ray tracing program
// Copyright (C) 2008-2014 Eric Bechet
//
// See the COPYING file for contributions and license information.
// Please report all bugs and problems to <bechet@cadxfem.org>.
//

#include "glframebuffer.h"
#include "glwindow.h"

void gl_framebuffer::get_coords ( float i, float j, Ray &r )
{
  ogl->get_coords ( i,j,r );
}
// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
