// Gray - A simple ray tracing program
// Copyright (C) 2008-2014 Eric Bechet
//
// See the COPYING file for contributions and license information.
// Please report all bugs and problems to <bechet@cadxfem.org>.
//

#ifndef __GLU_GRAY_H_
#define __GLU_GRAY_H_
#include <vector>
#ifdef WIN32
#include "windows.h"
#endif //WIN32
#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif //_APPLE_
#include "ray.h"



struct tri
{
  unsigned int p[3];
  tri ( unsigned int a,unsigned int b,unsigned int c )
  {
    p[0]=a;
    p[1]=b;
    p[2]=c;
  }
};

struct quad
{
  unsigned int p[4];
  quad ( unsigned int a, unsigned int b, unsigned int c, unsigned int d )
  {
    p[0] = a;
    p[1] = b;
    p[2] = c;
    p[3] = d;
  }
};

class glmeshbuffer
{
  std::vector<Point> vertices;
  std::vector<Point> normals;
  std::vector<tri> triangles;
  std::vector<quad> quads;
public:
  glmeshbuffer ( int nv,int nt, int nq = 0 );
  int add_vertex ( Point v,Point n );
  int add_triangle ( int a,int b,int c );
  int add_quad ( int a, int b, int c, int d );
  void draw ( void );
};


int gray_gluUnProject ( GLdouble winx, GLdouble winy, GLdouble winz,
                        const GLdouble modelMatrix[16],
                        const GLdouble projMatrix[16],
                        const GLint viewport[4],
                        GLdouble *objx, GLdouble *objy, GLdouble *objz );
int gray_gluUnProjectFast ( GLdouble winx, GLdouble winy, GLdouble winz,
                            const GLdouble Inv[16],
                            const GLint viewport[4],
                            GLdouble *objx, GLdouble *objy, GLdouble *objz );


void gray_gluMultMatricesd ( const GLdouble a[16], const GLdouble b[16],GLdouble r[16] );
void gray_gluMultMatrixVecd ( const GLdouble matrix[16], const GLdouble in[4],GLdouble out[4] );
int gray_gluInvertMatrixd ( const GLdouble src[16], GLdouble inverse[16] );
void gray_gluMakeIdentityd ( GLdouble m[16] );
void gray_gluPerspective ( GLdouble fovyInDegrees, GLdouble aspectRatio,
                           GLdouble znear, GLdouble zfar );
void gray_gluLookAt ( GLdouble *eyePosition3D,
                      GLdouble *center3D, GLdouble *upVector3D );


#endif // __GLU_GRAY_H_

// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
