// Gray - A simple ray tracing program
// Copyright (C) 2008-2014 Eric Bechet
//
// See the COPYING file for contributions and license information.
// Please report all bugs and problems to <bechet@cadxfem.org>.
//

#include <iostream>
#include <fstream>
#include <iomanip>
#include <math.h> //For atan
#ifdef WIN32
#include "windows.h"
#endif //WIN32
#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif //_APPLE_

#include "glwindow.h"
#include "glu_gray.h"

MyGlWindow::MyGlWindow ( int x, int y, int w, int h,gl_framebuffer *fb_ ) : Fl_Gl_Window ( x,y,w,h ),fb ( fb_ )
{
    fb->setogl ( this );
}

void MyGlWindow::draw_model()
{
    Gscene * sc = fb->getTimeLine()->getCurrentScene();

    if ( sc )
        for ( Gscene::iteratorModel it= sc->beginModel(); it!=sc->endModel(); ++it )
            (*it).second->draw_opengl();
}

void MyGlWindow::add_lights()
{
    int i=0;
    Gscene * sc = fb->getTimeLine()->getCurrentScene();

    if ( sc ) for ( Gscene::iteratorLight it= sc->beginLight(); it!=sc->endLight(); ++it,++i )
    {
        if ( i>=7 ) break;

        ( *it )->draw_opengl ( i );
    }
}

void MyGlWindow::draw_pixmap()
{
    if ( fb->ok() )
    {
        glMatrixMode ( GL_PROJECTION );
        glPushMatrix();
        glLoadIdentity();
        glOrtho ( 0,w(),0,h(),0,1 );
        glMatrixMode ( GL_MODELVIEW );
        glPushMatrix();
        glLoadIdentity();
        glRasterPos2i ( 0, 0 );
        glDrawPixels ( fb->getwidth(), fb->getheight(), GL_RGB, GL_FLOAT, fb->getbuffer() );
        glPopMatrix();
        glMatrixMode ( GL_PROJECTION );
        glPopMatrix();
    }
}

void MyGlWindow::print_info()
{
    std::cout << "Modelview matrix" << std::endl;

    for ( int i=0; i<4; ++i )
    {
        for ( int j=0; j<4; ++j )
            std::cout << std::setw ( 10 ) << ModelViewMatrix[i][j] ;

        std::cout << std::endl;
    }

    std::cout << "Projection matrix" << std::endl;

    for ( int i=0; i<4; ++i )
    {
        for ( int j=0; j<4; ++j )
            std::cout << std::setw ( 10 ) << ProjectionMatrix[i][j] ;

        std::cout << std::endl;
    }

    std::cout << "Viewport parameters" << std::endl;

    for ( int i=0; i<4; ++i )
        std::cout << std::setw ( 10 ) << Viewport[i] ;

    std::cout << std::endl;
    char * vers = ( char * ) glGetString ( GL_VERSION );

    if ( vers )
        std::cout << "OpenGL version : " <<   vers << std::endl ;

    std::cout << std::endl;
}

void MyGlWindow::get_coords ( GLfloat i,GLfloat j, Ray & r )
{
    gray_gluUnProjectFast ( i*1.0,j*1.0,0., ( GLdouble* ) Inv, ( GLint * ) Viewport,&r.p.X, &r.p.Y, &r.p.Z );
    gray_gluUnProjectFast ( i*1.0,j*1.0,1., ( GLdouble* ) Inv, ( GLint * ) Viewport, &r.v.X, &r.v.Y, &r.v.Z );
    r.v=r.p-r.v;
    r.v.normalize();
}

void MyGlWindow::get_matrices()
{
    glGetDoublev ( GL_MODELVIEW_MATRIX, ( GLdouble * ) ModelViewMatrix );
    glGetDoublev ( GL_PROJECTION_MATRIX, ( GLdouble * ) ProjectionMatrix );
    glGetIntegerv ( GL_VIEWPORT, ( GLint * ) Viewport );
    gray_gluMultMatricesd ( ( GLdouble * ) ModelViewMatrix, ( GLdouble * )  ProjectionMatrix, ( GLdouble * ) Inv );
    gray_gluInvertMatrixd ( ( GLdouble * ) Inv, ( GLdouble * ) Inv );

}

void MyGlWindow::set_view()
{
    Gscene * sc = fb->getTimeLine()->getCurrentScene();
    Camera *cam = sc->getCam();

    glLoadIdentity();
    glViewport ( 0,0,w(),h() );
    glOrtho ( 0,w(),0,h(),0,1 );

    // couleur de fond (RGBT)
    glClearColor ( 0.0, 0.0, 0.0, 0.0 );

    // Remplissage
    //  glShadeModel ( GL_FLAT);
    glShadeModel ( GL_SMOOTH );
    //  glEnable(GL_COLOR_MATERIAL);


    glPixelStorei ( GL_UNPACK_ALIGNMENT, 1 );
    glMatrixMode ( GL_PROJECTION );
    glLoadIdentity(); /* init projection matrix */

    double lens = cam->getLens();
    double focal = cam->getFocal();

    double fov = atan ( lens/2/focal ) * 2 * 180 / 3.14159265;

    if ( fov < 0 || fov > 180 )
        std::cout << "Warning : fov angle is probably not valid !" << std::endl;

    /* perspective parameters: field of view, aspect, near clip, far clip */
    gray_gluPerspective ( fov, ( GLdouble ) w() / ( GLdouble ) h(),
                         cam->getNearclip(), cam->getFarclip() );

    /* set matrix mode to modelview */
    glMatrixMode ( GL_MODELVIEW );
    glLoadIdentity();
    Point viewpoint = sc->getViewpoint(); // eye position

    GLdouble vp[3];
    vp[0]=viewpoint.X;
    vp[1]=viewpoint.Y;
    vp[2]=viewpoint.Z;

    GLdouble look[3];
    look[0] = cam->getDirection().X;
    look[1] = cam->getDirection().Y;
    look[2] = cam->getDirection().Z;

    GLdouble up[3];
    up[0] = cam->getUp().X;
    up[1] = cam->getUp().Y;
    up[2] = cam->getUp().Z;

    gray_gluLookAt ( vp,
                    look,    /* lookat point */
                    up );  /* up is in +ive y */
    glEnable ( GL_DEPTH_TEST );
    glPolygonMode ( GL_FRONT_AND_BACK,GL_FILL );
    glDepthFunc ( GL_LESS );
    glEnable ( GL_LIGHTING ) ;

    add_lights();


}

void MyGlWindow::draw()
{
    if ( !valid() )
    {
        valid ( 1 );
        set_view();
        get_matrices();
        //print_info();
    }

    glClear ( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    draw_model();
    draw_pixmap();
    glFlush();
};

void MyGlWindow::resize ( int X,int Y,int W,int H )
{
    Fl_Gl_Window::resize ( X,Y,W,H );
    fb->resize ( w(),h() );
};

int MyGlWindow::handle ( int event )
{
    TimeLine* tl = fb->getTimeLine();
    Gscene * sc = tl->getCurrentScene();


    switch ( event )
    {
        case FL_PUSH:
            //    ... mouse down event ...
            //    ... position in Fl::event_x() and Fl::event_y()
        {
            fb->resize ( w(),h() );
            fb->makeimage();
            //std::ofstream file("output.bin",std::ios::binary);
            //fb->save_image(file);
            //file.close();
            redraw();
            return 1;
        }

        case FL_DRAG:
            //    ... mouse moved while down event ...
            return 1;

        case FL_RELEASE:
            //    ... mouse up event ...
        {
            std::cout << "release" << std::endl;
            Ray r;
            std::cout << "screen coordinates : "  << Fl::event_x() << " " <<  Fl::event_y() << std::endl;
            get_coords ( Fl::event_x(),h()-Fl::event_y(), r );
            std::cout << "real coordinates : "   << r.p.X << " " <<r.p.Y<< " "  << r.p.Z << std::endl;
            std::cout << "vector : "  << r.v.X << " " <<r.v.Y<< " "  << r.v.Z << std::endl;
            return 1;
        }

        case FL_FOCUS :
        case FL_UNFOCUS :
            //    ... Return 1 if you want keyboard events, 0 otherwise
            return 1;

        case FL_MOUSEWHEEL:
        case FL_KEYBOARD:
        {
            switch ( Fl::event_key() )
            {
                case 65260 :      // MOUSEWHEEL en avant
                {
                    Point vp = sc->getViewpoint();
                    vp.Z -= 5;
                    refreshViewpoint ( vp );
                    break;
                }

                case 65261 :      // MOUSEWHEEL en arriere
                {
                    Point vp = sc->getViewpoint();
                    vp.Z += 5;
                    refreshViewpoint ( vp );
                    break;
                }

                case 65361 :      // KEY LEFT
                {
                    Point vp = sc->getViewpoint();
                    vp.X -= 5;
                    refreshViewpoint ( vp );
                    break;
                }

                case 65362 :      // KEY UP
                {
                    Point vp = sc->getViewpoint();
                    vp.Y += 5;
                    refreshViewpoint ( vp );
                    break;
                }

                case 65363 :      // KEY RIGHT
                {
                    Point vp = sc->getViewpoint();
                    vp.X += 5;
                    refreshViewpoint ( vp );
                    break;
                }

                case 65364 :      // KEY DOWN
                {
                    Point vp = sc->getViewpoint();
                    vp.Y -= 5;
                    refreshViewpoint ( vp );
                    break;
                }

                case 110 :        // N key
                {
                    Point vp = sc->getViewpoint();
                    tl->goToNextFrame();
                    sc = tl->getCurrentScene();

                    refreshViewpoint(vp);

                    /*
                    fb->resize ( w(),h() );
                    fb->makeimage();
                    */
                    //std::ofstream file("output.bin",std::ios::binary);
                    //fb->save_image(file);
                    //file.close();

                    redraw();
                    return 1;
                }

                case 112 :          // P key
                {
                    Point vp = sc->getViewpoint();
                    tl->goToPreviousFrame();
                    sc = tl->getCurrentScene();
                    refreshViewpoint(vp);
                    /*
                    fb->resize ( w(),h() );
                    fb->makeimage();
                    */
                    //std::ofstream file("output.bin",std::ios::binary);
                    //fb->save_image(file);
                    //file.close();

                    redraw();
                    return 1;

                }
                    
                case 97 : // A Key
                {
                    int currentFrame = tl->getCurrentFrame();
                    Point vp = sc->getViewpoint();
                    int frame = tl->goToFrame(0);
                    int frameCount = tl->getFramesCount();
                    
                    do {
                        
                        cout << endl << "### " << frame << " / " << frameCount << " ###" << endl;
                        
                        sc = tl->getCurrentScene();
                        refreshViewpoint(vp);
                        fb->resize ( w(),h() );
                        fb->makeimage();
                        
                        //redraw();
                        
                        std::stringstream sstm;
                        sstm << "Movie/Scene-" << std::setfill('0') << std::setw(4) <<  frame << ".png" ;
                        const char* filename = sstm.str().c_str();
                        
                        cout << "Saving file '" << filename << "' ...";
                        
                        fb->save_image_png(filename);
                        
                        cout << " Ok" << endl;
                    }
                    while ((frame = tl->goToNextFrame()) != -1);
                    
                    tl->goToFrame(currentFrame);
                    sc = tl->getCurrentScene();
                    refreshViewpoint(vp);
                    
                    return 1;
                }
            }

            //    ... keypress, key is in Fl::event_key(), ascii in Fl::event_text()
            //    ... Return 1 if you understand/use the keyboard event, 0 otherwise...
            return 1;
        }

        case FL_SHORTCUT:
            //    ... shortcut, key is in Fl::event_key(), ascii in Fl::event_text()
            //    ... Return 1 if you understand/use the shortcut event, 0 otherwise...
            return 1;

        default:
            // pass other events to the base class...
            return Fl_Gl_Window::handle ( event );
    }
}

void MyGlWindow::refreshViewpoint ( Point newViewpoint )
{
    Gscene * sc = fb->getTimeLine()->getCurrentScene();

    sc->setViewpoint ( newViewpoint );
    valid ( 0 );
    fb->clear();
    //  fb->makeimage();
    redraw();
}
// kate: indent-mode cstyle; indent-width 2; replace-tabs on;
