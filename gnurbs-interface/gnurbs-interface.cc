// Gray - A simple ray tracing program
// Copyright (C) 2008-2014 Eric Bechet
//
// See the COPYING file for contributions and license information.
// Please report all bugs and problems to <bechet@cadxfem.org>.
//

#include "nbeziercurve.h"
#include "nutil.h"
#include "gnurbs-interface.h"
#include <iostream>

void test ( void )
{
  nbeziercurve bez ( 5 );
  bez.CP ( 0 ) =npoint ( 0,0,0 );
  bez.CP ( 1 ) =npoint ( 1,0,0 );
  bez.CP ( 2 ) =npoint ( 1,1,0 );
  bez.CP ( 3 ) =npoint ( 0,2,0 );
  bez.CP ( 4 ) =npoint ( 0,3,0 );
  npoint res;
  bez.P ( 0.5,res );
  std::cout << res[0] << " " << res[1] << " " << res[2] << std::endl;
}
// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
