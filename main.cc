// Gray - A simple ray tracing program
// Copyright (C) 2008-2014 Eric Bechet
//
// See the COPYING file for contributions and license information.
// Please report all bugs and problems to <bechet@cadxfem.org>.
//

#include "main.h"
#include "glframebuffer.h"
#include "glwindow.h"
#include "scene.h"
#include "materials.h"
#include "meshmodel.h"
#include <iostream>
#include <algorithm>
#include <vector>
#include "QuasiMonteCarlo.h"
#ifdef GRAY_HAVE_GNURBS
#include "gnurbs-interface.h"
#endif
#include "Texture.h"
#include "Transform.h"
#include "TimeLine.h"
#include "AutoReverseKeyFrame.h"
#include "ModelGroup.h"
#include "VertexGroup.h"

int main ( int argc, char *argv[] )
{
    
    int i = argc > 1 ? atoi(argv[1]) : 2;
    
    switch (i) {
        case 1:
            
            return testEasing();
            break;
            
        case 2:
            return testGroups();
            break;
        
        case 3:
            return testVertices();
            break;
            
        default:
            return 0;
    }
}

/**
 * D�mo des diff�rentes fonctions d'Easing
 */
int testEasing() {

	// Initialise la scene
    Gscene sc;
    sc.setOversampling ( 1 );
    sc.setMaxRecurLevel ( 200 );
    sc.setViewpoint ( Point ( 0,-300,600 ) );
    
    // Initialise les mat�riaux
    GmaterialLambertBlinnPhong mat0 ( Color ( 1.0,1.0,1.0 ), Color ( 0.6,0.4,0.2 ), 100., 4.0 );
    
    // Initialise une TimeLine de 200 frames se basant sur la scene 'sc'
    TimeLine t(200, sc);
    
    // Initialise les fonctions de easing
    
    LinearEasingFunction f1;
    QuadraticEasingFunction f2;
    CubicEasingFunction f3;
    QuadraticEasingFunction f4;
    QuinticEasingFunction f5;
    SineEasingFunction f6;
    CircularEasingFunction f7;
    ExponentialEasingFunction f8;
    ElasticEasingFunction f9;
    BackEasingFunction f10;
    BounceEasingFunction f11;
    
    EasingFunction* f[11] = {
        &f1, &f2, &f3, &f4, &f5, &f6, &f7, &f8, &f9, &f10, &f11
    };
    
    LinearEasingFunction l;

    //Initialise les transformations

    Transform transform = Transform::newTranslation(600, 0, 0);
    //Transform transform = Transform::newRotation(M_PI_2, 0, 0, 1);
    
    for (int i = 0; i < 11; i ++) {
        
        float offset = (11/2. - i) * 100 + 300;
        
        //Cr�e le mod�le
        Gsphere * sphere = new Gsphere(30.0,
                                       Point(-300, offset, 30),
                                       &mat0);
        
        //Ajoute le mod�le dans un groupe
        ModelGroup * sphereGroup = new ModelGroup(Point(0, offset, 0));
        sphereGroup->add(*sphere);
        
        //Ajoute le groupe � la scene
        sc.insertGroup(sphereGroup);

        //Ajoute le mod�le � la scene
        sc.insertModel(sphere);
        
        //Ajoute une keyframe � la scene (pour interpoler les mouvements)
        t.addKeyFrame(new AutoReverseKeyFrame(0,100,*sphereGroup, transform, *f[i]));
    }
    
    // Ajoute d'autres mod�les
    sc.insertModel ( new Gdisc ( 10000,Point ( 0,0,0 ), Point ( 0,0,1 ), &mat0 ) );
    
    // Ajoute les lampes
    Glight *light2 = new Glight ( Point ( 100,100,350 ), 1, Color ( 1.0,1.0,1.0 ) );
    sc.insertLight ( light2 );
    sc.setTypicalLightDist ( light2->getPosition().norm() );
    
    // Camera
    Camera *cam = new Camera();
    sc.setCam ( cam );
    cam->setFocusDist ( 22.36 );
    cam->setDirection ( Point ( 0,100,0 ) );

    
    gl_framebuffer *fb= new gl_framebuffer (200, 200, &t);
    
    Fl_Window *win =
    new Fl_Window(fb->getwidth(),
                  fb->getheight(),
                  "Gray - the homemade raytracer");
    
    MyGlWindow* gl = new MyGlWindow ( 0,0,win->w(), win->h(), fb );
    win->end();
    win->resizable ( gl );
    win->show();  // this actually opens the window
    
    if (false || sc.getCam() == NULL )
    {
        std::cout << "No camera has been configured. Interrupting." << std::endl;
        return 1;
    }
    
    Fl::run();
    
    delete cam;
    delete gl;
    delete win;
    delete fb;
    
    
    return 0;
}

/**
 * D�mo des ModelGroups
 */
int testGroups() {

	// Initialisation de la sc�ne

    Gscene sc;
    sc.setOversampling ( 1 );
    sc.setMaxRecurLevel ( 200 );
    sc.setViewpoint ( Point ( 0,0,100 ) );
    
    // Initialisation des mat�riaux

    GmaterialLambertBlinnPhong mat0 ( Color ( 1.0,1.0,1.0 ), Color ( 0.6,0.4,0.2 ), 100., 4. );
    
    // Chargement et initialisation d'un model de base (ply/os.ply)
    Gmeshmodel os(&mat0);
    os.linearInterpolation = false;
    os.read_ply("ply/os", 1,0, 0, 0);
    os.mergeCloseVertices();
    os.transform(Transform::identity);
    os.compute_normals();
    
    // Construction d'un bonhomme morceau par morceau :
    //   pour chaque partie du corps :
    //      - cloner le mod�le de base (os)
    //      - le placer o� l'on souhaite (transform)
    //      - l'ajouter � la sc�ne
    
    Gmeshmodel body("body", os);
    body.transform(Transform::newScale(100, 50, 50));
    sc.insertModel(&body);
    
    Gmeshmodel arml("arml", os);
    arml.transform(Transform::newScale(20).translate(-15, 30, 0));
    sc.insertModel(&arml);
    
    Gmeshmodel armr("armr", os);
    armr.transform(Transform::newScale(20).translate(15, 30, 0));
    sc.insertModel(&armr);
    
    Gmeshmodel forearml("forearml", os);
    forearml.transform(Transform::newScale(20).translate(-15, 10, 0));
    sc.insertModel(&forearml);
    
    Gmeshmodel forearmr("forearmr", os);
    forearmr.transform(Transform::newScale(20).translate(15, 10, 0));
    sc.insertModel(&forearmr);
    
    Gmeshmodel femurr("femurr", os);
    femurr.transform(Transform::newScale(40).translate(5, -40, 0));
    sc.insertModel(&femurr);
    
    Gmeshmodel femurl("femurl", os);
    femurl.transform(Transform::newScale(40).translate(-5, -40, 0));
    sc.insertModel(&femurl);
    
    Gmeshmodel tibiar("tibiar", os);
    tibiar.transform(Transform::newScale(30).translate(5, -65, 0));
    sc.insertModel(&tibiar);
    
    Gmeshmodel tibial("tibial", os);
    tibial.transform(Transform::newScale(30).translate(-5, -65, 0));
    sc.insertModel(&tibial);
    
    Gmeshmodel footl("footl", os);
    footl.transform(Transform::newRotation(M_PI_2, 1, 0, 0).scale(15).translate(-5, -65, 15));
    sc.insertModel(&footl);
    
    Gmeshmodel footr("footr", os);
    footr.transform(Transform::newRotation(M_PI_2, 1, 0, 0).scale(15).translate(5, -65, 15));
    sc.insertModel(&footr);
    
    Gsphere head("head", 8, Point(0, 60, 0), & mat0);
    sc.insertModel(&head);
    
    // ----------------------------------------------
    
    GmaterialLambertBlinnPhong mat1(Color(0.2,0.2,0.2), Color(0.2,0.2,0.2), 100., 0. );
    sc.insertModel ( new Gdisc ( 10000,Point ( 0,0,-20 ), Point ( 0,0,1 ), &mat1 ) );

    // ----------------------------------------------
    
    Gsphere test("test", 1, Point(15,30,0), &mat1);
    sc.insertModel(&test);
    
    // Initialisation des ModelGroups

    /* Pour chaque partie du corps :
     *
     * 	- Cr�er un groupe,
     * 	- lui ajouter les mod�les correspondants (doivent �tre d�j� ajout�s � la sc�ne)
     * 	- lui ajouter �ventuelement les sous-groupes
     * 	- ajouter le groupe � la sc�ne
     */

    ModelGroup forearmlGroup("forearmlGroup", Point(-15, 30, 0));
    forearmlGroup.add(forearml);
    sc.insertGroup(&forearmlGroup);
    
    ModelGroup forearmrGroup("forearmrGroup", Point(15, 30, 0));
    forearmrGroup.add(forearmr);
    sc.insertGroup(&forearmrGroup);
    
    ModelGroup armlGroup("armlGroup", Point(-15, 50, 0));
    armlGroup.add(forearmlGroup);
    armlGroup.add(arml);
    sc.insertGroup(&armlGroup);
    
    ModelGroup armrGroup("armrGroup", Point(15, 50, 0));
    armrGroup.add(forearmrGroup);
    armrGroup.add(armr);
    armrGroup.addModel(test);
    sc.insertGroup(&armrGroup);
    
    ModelGroup headGroup("headGroup", Point(0, 50, 0));
    headGroup.add(head);
    sc.insertGroup(&headGroup);
    
    ModelGroup bodyGroup("bodyGroup", Point(0, 0, 0));
    bodyGroup.add(headGroup);
    bodyGroup.add(armlGroup);
    bodyGroup.add(armrGroup);
    bodyGroup.add(body);
    sc.insertGroup(&bodyGroup);
    
    // ----------------------------------------------
    
    Glight *light = new Glight ( Point ( -100,140,600 ), 1, Color ( 1.0,1.0,1.0 ) );
    sc.insertLight(light);
    sc.setTypicalLightDist(light->getPosition().norm());
    
    Glight *light2 = new Glight ( Point ( 100,100,350 ), 1, Color ( 1.0,1.0,1.0 ) );
    sc.insertLight ( light2 );
    sc.setTypicalLightDist ( light2->getPosition().norm() );
    
    // ----------------------------------------------

    // Camera
    Camera *cam = new Camera();
    sc.setCam ( cam );
    cam->setFocusDist ( 22.36 );
    cam->setDirection ( Point ( 0,0,0 ) );
    
    // -----------------------------------------------

    // initialise une TimeLine de 70 frames se basant sur la sc�ne 'sc'

    TimeLine t(70, sc);
    
    CubicEasingFunction l(EaseInOut);
    
    Transform rx90 = Transform::newRotation(M_PI_2, 1, 0, 0);
    Transform rx_45 = Transform::newRotation(-M_PI_4, 1, 0, 0);
    
    // Cr��e les keyFrames pour animer les models

    SimpleKeyFrame kf (0, 10, armrGroup, rx90, l);
    SimpleKeyFrame kf2(11, 9, forearmrGroup, rx90, l);
    SimpleKeyFrame kf3(20, 10, armrGroup, rx_45, l);
    SimpleKeyFrame kf4(30, 10, headGroup, rx_45, l);
    SimpleKeyFrame kf5(40, 10, bodyGroup, rx_45, l);
    SimpleKeyFrame kf6(60, 10, bodyGroup, Transform::newRotation(-M_PI_4, 0, 1, 0), l);
    
    // Ajoute les keyFrames � la timeLine

    t.addKeyFrame(&kf);
    t.addKeyFrame(&kf2);
    t.addKeyFrame(&kf3);
    t.addKeyFrame(&kf4);
    t.addKeyFrame(&kf5);
    t.addKeyFrame(&kf6);
    
    gl_framebuffer *fb= new gl_framebuffer (600, 600, &t);
    
    Fl_Window *win =
    new Fl_Window(fb->getwidth(),
                  fb->getheight(),
                  "Gray - the homemade raytracer");
    
    MyGlWindow* gl = new MyGlWindow ( 0,0,win->w(), win->h(), fb );
    win->end();
    win->resizable ( gl );
    win->show();  // this actually opens the window
    
    if (false || sc.getCam() == NULL )
    {
        std::cout << "No camera has been configured. Interrupting." << std::endl;
        return 1;
    }
    
    Fl::run();
    
    delete cam;
    delete gl;
    delete win;
    delete fb;
    
    
    return 0;

}

/**
 * D�mo d'animation de vertex d'un gmeshmodel
 */
int testVertices() {

    //Textures loading
    /*
     Texture tex1 ( "textures/checker.bmp" );
     Texture tex2 ( "textures/ground.png" );
     Texture tex3 ( "textures/checkergreen.png" );
     Texture tex4 ( "textures/checker_normal.png" );
     Texture tex5 ( "textures/checkergreen_normal.png" );
     Texture tex6 ( "textures/ground_normal.png" );
     */
    
    // Various materials
    GmaterialTransparent mat0 ( Color ( 0,0,0 ), Color ( 0,0,0 ), Color ( 0.9931,0.9931,1 ), 100, 4, 1.52 );
    /*  GmaterialLambertBlinnPhong mat1(Color(1.0,1.0,1.0), Color(0.3,0.3,0.3), 100., 4.0);
     GmaterialLambertBlinnPhong mat2(Color(1.0,1.0,1.0), Color(0.3,0.3,0.3), 100., 4.0);
     GmaterialLambertBlinnPhong mat3(Color(1.0,1.0,1.0), Color(0.3,0.3,0.3), 100., 4.0);
     GmaterialLambertBlinnPhong mat4(Color(1.0,1.0,1.0), Color(0.3,0.3,0.3), 100., 4.0);
     GmaterialLambertBlinnPhong mat5(Color(1,1,1), Color(0.1,0.1,0.1), 100., 0);*/
    GmaterialLambertBlinnPhong mat1 ( Color ( 1.0,1.0,0.0 ), Color ( 0.3,0.3,0.3 ), 100., 4.0 );
    GmaterialLambertBlinnPhong mat2 ( Color ( 0.0,1.0,1.0 ), Color ( 0.3,0.3,0.3 ), 100., 4.0 );
    GmaterialLambertBlinnPhong mat3 ( Color ( 1.0,1.0,1.0 ), Color ( 1.0,1.0,1.0 ), 100., 4.0 );
    GmaterialLambertBlinnPhong mat4 ( Color ( 0.0,0.0,0.0 ), Color ( 0.3,0.3,0.3 ), 100., 4.0 );
    GmaterialLambertBlinnPhong mat5 ( Color ( 1.0,1.0,1.0 ), Color ( 0.1,0.1,0.1 ), 100., 0 );
    
    
    
    
    GmaterialLambertBlinnPhong mat6 ( Color ( 111./255.,49./255.,152./255. ),Color ( 0.3,0.3,0.3 ), 12.,1.0 );
    
    Gscene sc;
    sc.setOversampling ( 1 );
    sc.setMaxRecurLevel ( 200 );
    sc.setViewpoint ( Point ( 0,0,100 ) );
    //sc.setPhotonLighting(false);
    
    //GmaterialLambertBlinnPhong mat0 ( Color ( 1.0,1.0,1.0 ), Color ( 0.6,0.4,0.2 ), 100., 4. );
    
    //Charge un meshmodel
    
    Gmeshmodel snake(&mat1);
    snake.linearInterpolation = true;
    snake.read_ply("ply/snake", 10,0, 0, 0);
    snake.mergeCloseVertices();
    snake.transform(Transform::identity);
    snake.compute_normals();

    //Ajouter le meshmodel � la sc�ne

    sc.insertModel(&snake);
    
    // ----------------------------------------------
    
    // On va cr�er 5 groupes de vertex :

    VertexGroup* vg[5];
    VertexGroup* prev = NULL;
    
    for (int i = 5; i >= 0; i--) {


        Point center(0,20 * i,0);
        
        // cr�e un VertexGroup de centre 'center' se basant sur le meshmodel 'snake'
        vg[i] = new VertexGroup(center, snake);
        
        // r�cup�re les indices des points se situant dans une sphere de rayon 20
        // de centre 'center'
        std::vector<int> indexes = snake.getVerticeIndexes(center, 20.0);
        
        // Ajoute les poins au VertexGroup
        for (int p = 0; p < indexes.size(); p ++)
            vg[i]->add(indexes[p]);
        
        // Ajoute le groupe pr�c�dent comme sous-groupe de l'actuel
        // (pour les chainer --> si on modifie le premier, il modifie tout le mod�le)
        if (prev)
            vg[i]->add(*prev);
        
        // Ajoute le groupe actuel � la sc�ne
        sc.insertGroup(vg[i]);
        
        prev = vg[i];
    }
        
    // ----------------------------------------------
    
    GmaterialLambertBlinnPhong matDisk(Color(0.2,0.2,0.2), Color(0.2,0.2,0.2), 100., 0. );
    sc.insertModel ( new Gdisc ( 10000,Point ( 0,0,-20 ), Point ( 0,0,1 ), &matDisk ) );
    
    // ----------------------------------------------
    
    Glight *light = new Glight ( Point ( -300,100, 0 ), 1, Color ( 1.0,1.0,1.0 ) );
    sc.insertLight(light);
    sc.setTypicalLightDist(light->getPosition().norm());
    
    Glight *light2 = new Glight ( Point ( 100,100,350 ), 1, Color ( 1.0,1.0,1.0 ) );
    sc.insertLight ( light2 );
    sc.setTypicalLightDist ( light2->getPosition().norm() );
    
    // Camera
    Camera *cam = new Camera();
    sc.setCam ( cam );
    cam->setFocusDist ( 22.36 );
    cam->setDirection ( Point ( 0,0,0 ) );
    
    // Initialise une TimeLine de 100 frames
    
    TimeLine t(100, sc);
    
    BounceEasingFunction l;
    
    // Fait tourn� chaque groupe de 10 degr�s autour de l'axe Z
    // et 30 degr�s autour de l'axe Y

    for (int i = 0; i < 5; i++) {
        
        //int s = (i % 2) ? -1 : 1;
        Transform transform = Transform::newRotation(DegToRad(-10), 0, 0, 1).rotate(DegToRad(-30), 0, 1, 0);
        

        SimpleKeyFrame * kf = new SimpleKeyFrame(0, 100, *vg[i], transform, l);
        t.addKeyFrame(kf);
    }
  
    // Applique en plus une rotation du mod�le 'snake' de 90 degr�s autour de l'axe X

    ModelGroup snakeGroup("snakeGroup", Point(-45, 0, 0));
    snakeGroup.add(snake);
    
    sc.insertGroup(&snakeGroup);
    SimpleKeyFrame kf2(0, 100, snakeGroup, Transform::newRotation(M_PI_2, 0, 0, 1), l);
    t.addKeyFrame(&kf2);
    
    gl_framebuffer *fb= new gl_framebuffer (600, 600, &t);
    
    Fl_Window *win =
    new Fl_Window(fb->getwidth(),
                  fb->getheight(),
                  "Gray - the homemade raytracer");
    
    MyGlWindow* gl = new MyGlWindow ( 0,0,win->w(), win->h(), fb );
    win->end();
    win->resizable ( gl );
    win->show();  // this actually opens the window
    
    if (false || sc.getCam() == NULL )
    {
        std::cout << "No camera has been configured. Interrupting." << std::endl;
        return 1;
    }
    
    Fl::run();
    
    delete cam;
    delete gl;
    delete win;
    delete fb;
    
    
    return 0;
}
