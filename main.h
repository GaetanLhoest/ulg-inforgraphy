// Gray - A simple ray tracing program
// Copyright (C) 2008-2014 Eric Bechet
//
// See the COPYING file for contributions and license information.
// Please report all bugs and problems to <bechet@cadxfem.org>.
//

#ifndef __MAIN_H_
#define __MAIN_H_

int main ( int argc, char *argv[] );

int testEasing();
int testGroups();
int testVertices();

#endif //__MAIN_H_
// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
