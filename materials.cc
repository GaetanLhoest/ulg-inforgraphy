// Gray - A simple ray tracing program
// Copyright (C) 2008-2014 Eric Bechet
//
// See the COPYING file for contributions and license information.
// Please report all bugs and problems to <bechet@cadxfem.org>.
//

#include <cmath>
#include <cstdlib>
#include <iostream>

#include "materials.h"
#include "scene.h"

#ifdef GRAY_HAVE_OGL
#ifdef WIN32
#include "windows.h"
#endif //WIN32
#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include "GL/gl.h"
#endif
#endif// GRAY_HAVE_OGL

#define MAX(a,b) a > b? a : b

void GmaterialLambertBlinnPhong::draw_opengl ( void )
{
#ifdef GRAY_HAVE_OGL
  GLfloat matAmb[] = {0.0, 0.0, 0.0, 0.0};
  GLfloat matDiff[] = {color.red, color.green, color.blue, 1.0};
  GLfloat matSpec[] = {phong, phong, phong, 1.0};
  GLfloat matEmission[] = {0.0, 0.0, 0.0, 0.0};
  glMaterialfv ( GL_FRONT_AND_BACK, GL_AMBIENT, matAmb );
//  glEnable(GL_COLOR_MATERIAL);        // Configure glColor().
  glColorMaterial ( GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE );
  glMaterialfv ( GL_FRONT_AND_BACK, GL_DIFFUSE, matDiff );
//  glColor3f(color.red, color.green, color.blue);
  glMaterialfv ( GL_FRONT_AND_BACK, GL_SPECULAR, matSpec );
//  glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, matEmission);
  glMaterialf ( GL_FRONT_AND_BACK, GL_SHININESS, shininess );
#endif// GRAY_HAVE_OGL
}

void GmaterialLambertBlinnPhong::brdf ( Point from,Point n,Point to,Color& factors )
{
  factors = Color ( 0,0,0 );
  //Lambert
  float cosI = - ( from*n );

  if ( cosI > 0 ) factors = factors + color * cosI;

  //Phong
  Point h = from + to;
  h.normalize();
  float nDotH = - ( n*h );
  float posnDotH = MAX ( 0.0, nDotH );
  float phongTerm = powf ( posnDotH , shininess ) * phong;
  factors = factors + rcolor * phongTerm;
}

void GmaterialLambertBlinnPhong::brdf ( Point from,Point n,Point to, Color localColor, Color& factors )
{
  factors = Color ( 0,0,0 );
  //Lambert
  float cosI = - ( from*n );

  if ( cosI > 0 ) factors = factors + localColor * cosI;

  //Phong
  Point h = from + to;
  h.normalize();
  float nDotH = - ( n*h );
  float posnDotH = MAX ( 0.0, nDotH );
  float phongTerm = powf ( posnDotH , shininess ) * phong;
  factors = factors + rcolor * phongTerm;
}

bool GmaterialLambertBlinnPhong::getNextRay ( HitInfo &hitInfo, Ray &nextRay, Gscene &scene )
{
  nextRay.kfactor = hitInfo.ray.kfactor * rcolor;

  if ( nextRay.kfactor.isBlack() ) return false;

  nextRay.p = hitInfo.point + hitInfo.normal*1E-6; //légérement à l'extérieur
  nextRay.v = hitInfo.ray.v - hitInfo.normal*2* ( hitInfo.normal*hitInfo.ray.v );
  nextRay.level = hitInfo.ray.level + 1;
  return true;
}

void GmaterialTransparent::draw_opengl ( void )
{
#ifdef GRAY_HAVE_OGL
  GLfloat matAmb[] = {0.0, 0.0, 0.0, 0.0};
  GLfloat matDiff[] = {dColor.red+vColor.red, dColor.green+vColor.green, dColor.blue+vColor.blue, 1.0};
  GLfloat matSpec[] = {phongCoef, phongCoef, phongCoef, 1.0};
  GLfloat matEmission[] = {0.0, 0.0, 0.0, 0.0};
  glMaterialfv ( GL_FRONT_AND_BACK, GL_AMBIENT, matAmb );
//  glEnable(GL_COLOR_MATERIAL);        // Configure glColor().
  glColorMaterial ( GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE );
  glMaterialfv ( GL_FRONT_AND_BACK, GL_DIFFUSE, matDiff );
//  glColor3f(color.red, color.green, color.blue);
  glMaterialfv ( GL_FRONT_AND_BACK, GL_SPECULAR, matSpec );
//  glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, matEmission);
  glMaterialf ( GL_FRONT_AND_BACK, GL_SHININESS, phongExp );
#endif// GRAY_HAVE_OGL
}

void GmaterialTransparent::brdf ( Point from,Point n,Point to,Color& factors )
{
  factors = Color ( 0,0,0 );
  //Lambert
  float cosI = - ( from*n );

  if ( cosI > 0 ) factors = factors + dColor * cosI;

  //Phong
  Point h = from + to;
  h.normalize();
  float nDotH = - ( n*h );
  float posnDotH = MAX ( 0.0, nDotH );
  float phongTerm = powf ( posnDotH , phongExp ) * phongCoef;
  factors = factors + rColor * phongTerm;
}

void GmaterialTransparent::brdf ( Point from,Point n,Point to,Color localColor, Color& factors )
{
  factors = Color ( 0,0,0 );
  //Lambert
  float cosI = - ( from*n );

  if ( cosI > 0 ) factors = factors + dColor * cosI;

  //Phong
  Point h = from + to;
  h.normalize();
  float nDotH = - ( n*h );
  float posnDotH = MAX ( 0.0, nDotH );
  float phongTerm = powf ( posnDotH , phongExp ) * phongCoef;
  factors = factors + rColor * phongTerm;
}

bool GmaterialTransparent::getNextRay ( HitInfo &hitInfo, Ray &nextRay, Gscene &scene )
{
  if ( !isReflective() && !isTransmissive() ) // no output ray
    return false;

  double cosI = hitInfo.normal * hitInfo.ray.v;
  double n1,n2;
  Point normal;

  if ( cosI < 0 ) // we come from outside
  {
    n1 = scene.getRefractiveIndex();
    n2 = n;
    cosI = -cosI;
    normal = hitInfo.normal;
  }
  else
  {
    n1 = n;
    n2 = scene.getRefractiveIndex();
    normal = hitInfo.normal *-1;

    //Beer law
    hitInfo.ray.kfactor.red *= powf ( vColor.red, hitInfo.distance );
    hitInfo.ray.kfactor.green *= powf ( vColor.green, hitInfo.distance );
    hitInfo.ray.kfactor.blue *= powf ( vColor.blue, hitInfo.distance );
  }

  double reflectance, cosT;

  if ( cosI > 0.9999 )
  {
    cosI = 1;
    cosT = 1;
    reflectance = ( n1-n2 ) / ( n1+n2 );
    reflectance = reflectance*reflectance;
  }
  else
  {
    double sinT2 = ( n1/n2 ) * ( n1/n2 ) * ( 1-cosI*cosI );

    if ( sinT2 > 0.9999 )
    {
      reflectance = 1;
    }
    else
    {
      cosT = sqrt ( 1 - sinT2 );

      float reflectanceOrtho = ( n2*cosT-n1*cosI ) / ( n2*cosT+n1*cosI );
      reflectanceOrtho *= reflectanceOrtho;
      float reflectanceParal = ( n1*cosT-n2*cosI ) / ( n1*cosT+n2*cosI );
      reflectanceParal *= reflectanceParal;
      reflectance = ( reflectanceOrtho + reflectanceParal ) /2;
    }
  }

  if ( !isReflective() ) //only refracted rays
  {
    nextRay.v = hitInfo.ray.v * ( n1 / n2 ) + normal * ( n1 / n2 * cosI - cosT );
    nextRay.kfactor = hitInfo.ray.kfactor * ( 1-reflectance );
  }
  else if ( !isTransmissive() ) // only reflected rays
  {
    nextRay.v = hitInfo.ray.v + normal*2*cosI;
    nextRay.kfactor = hitInfo.ray.kfactor * rColor * reflectance;
  }
  else // both rays -> russian roulette
  {
    if ( rand() < reflectance * RAND_MAX ) // ray is reflected
    {
      nextRay.v = hitInfo.ray.v + normal*2*cosI;
      nextRay.kfactor = hitInfo.ray.kfactor * rColor;
    }
    else // ray is refracted
    {
      nextRay.v = hitInfo.ray.v * ( n1 / n2 ) + normal * ( n1 / n2 * cosI - cosT );
    }
  }

  nextRay.p = hitInfo.point + nextRay.v * 1E-6;
  nextRay.level = hitInfo.ray.level + 1;

  return true;
}


// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
