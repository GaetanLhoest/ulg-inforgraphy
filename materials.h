// Gray - A simple ray tracing program
// Copyright (C) 2008-2014 Eric Bechet
//
// See the COPYING file for contributions and license information.
// Please report all bugs and problems to <bechet@cadxfem.org>.
//

#ifndef __MATERIALS_H_
#define __MATERIALS_H_

#include "ray.h"

//Texture support
#include "Texture.h"

class Gscene;
class HitInfo;

class Gmaterial
{
public:
  Gmaterial ( bool diffuse, bool reflect, bool transmit ) : diffuse ( diffuse ), reflect ( reflect ), transmit ( transmit ), texture ( 0 ), normalMap ( 0 ) {}
  virtual ~Gmaterial() {}

  virtual Color getColor() = 0; // for fast opengl drawing
  virtual void draw_opengl() = 0; // for opengl drawing

  virtual void brdf ( Point from,Point n,Point to,Color& factors ) = 0; // bidirectional reflectance distribution function
  virtual void brdf ( Point from,Point n,Point to, Color localColor, Color& factors ) = 0;
  virtual bool getNextRay ( HitInfo &hitInfo, Ray &nextRay, Gscene &scene ) = 0;

  // When shading, it's or not necessary to calculate corresponding color?
  bool isDiffusive()
  {
    return diffuse;
  }
  bool isReflective()
  {
    return reflect;
  }
  bool isTransmissive()
  {
    return transmit;
  }

  //Texture support
  void setTexture ( Texture* texture, double uTile = 1.0, double vTile = 1.0 )
  {
    this->texture = texture;
    this->uTile = uTile;
    this->vTile = vTile;
  }
  Texture* getTexture() const
  {
    return texture;
  }
  void setNormalMap ( Texture* normalMap )
  {
    this->normalMap = normalMap;
  }
  Texture* getNormalMap() const
  {
    return normalMap;
  }

  double getUTile() const
  {
    return uTile;
  }
  double getVTile() const
  {
    return vTile;
  }

private :
  bool diffuse;
  bool reflect;
  bool transmit;

  //Texture support
  Texture* texture;
  Texture* normalMap;
  double uTile, vTile;
};

class GmaterialLambertBlinnPhong : public Gmaterial
{
public :
  GmaterialLambertBlinnPhong ( Color c_,Color rc_,float shininess_,float phong_ ) :
    Gmaterial ( !c_.isBlack(), !rc_.isBlack() && ( phong>0 ), false ), color ( c_ ),rcolor ( rc_ ), shininess ( shininess_ ), phong ( phong_ ) {}
  virtual ~GmaterialLambertBlinnPhong() {}

  virtual Color getColor()
  {
    return color;
  }
  virtual void draw_opengl ( void ) ;

  virtual void brdf ( Point from,Point n,Point to,Color& factors );
  virtual void brdf ( Point from,Point n,Point to, Color localColor, Color& factors );
  virtual bool getNextRay ( HitInfo &hitInfo, Ray &nextRay, Gscene &scene );

private:
  Color color; // diffuse color (lambert reflectance)
  Color rcolor; // reflexion color coefficients
  float shininess; // phong exponent
  float phong; // phong coefficient
};

class GmaterialTransparent : public Gmaterial
{
public :
  GmaterialTransparent ( Color dColor, Color rColor, Color vColor, float phongExp, float phongCoef, float n ) :
    Gmaterial ( !dColor.isBlack(), !rColor.isBlack() && ( phongCoef>0 ), !vColor.isBlack() && ( n>0 ) ),
    dColor ( dColor ), rColor ( rColor ), vColor ( vColor ), phongCoef ( phongCoef ), phongExp ( phongExp ), n ( n ) {}
  virtual ~GmaterialTransparent() {}

  virtual Color getColor()
  {
    return dColor;
  }
  virtual void draw_opengl ( void ) ;

  virtual void brdf ( Point from,Point n,Point to,Color& factors );
  virtual void brdf ( Point from,Point n,Point to, Color localColor, Color& factors );
  virtual bool getNextRay ( HitInfo &hitInfo, Ray &nextRay, Gscene &scene );

private :
  Color dColor; // diffuse color (Lambert law)
  Color rColor; // reflexion color
  Color vColor; // volume color (Beer law)
  float phongCoef, phongExp; // specular reflexion (Phong law)
  float n; // refractive index
};

#endif //__MATERIALS_H_

// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
