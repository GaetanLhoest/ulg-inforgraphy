// Gray - A simple ray tracing program
// Copyright (C) 2008-2014 Eric Bechet
//
// See the COPYING file for contributions and license information.
// Please report all bugs and problems to <bechet@cadxfem.org>.
//

#ifndef __MESH_MODEL_H_
#define __MESH_MODEL_H_

#include <map>
#include <set>
#include <vector>
#include "models.h"

/**
 * Definition of a mesh model.
 * It accepts faces with 3 or 4 vertices (triangles or quad).
 *
 * How to use it :
 * - mandatory : specify material in the constructor;
 * - mandatory : use read_ply to load an existing mesh (only once);
 * - optional : use subdivideCatmulClark or subdivideSqrt3 to refine the model;
 * - mandatory : call compute_normals;
 * - optional : set linearInterpolation (default is true);
 * The model is ready.
 */
class Gmeshmodel : public Gmodel
{
    class Tree;
    
protected:
    
    virtual string getDefaultName();
    void recalc();
    
public:
    
    Gmeshmodel(const Gmeshmodel& m);
    Gmeshmodel(string newName, const Gmeshmodel& m);
    
    void transform(const Transform &t);
    void transform(unsigned int vertexIndex, const Transform &t);
    
    /**
     * true to compute a linear interpolation of the normal;
     * false to use the normal to the face.
     *
     * This option must be defined before using 'void compute_normals()' method.
     */
    bool linearInterpolation;
    
    /**
     * Constructor.
     * @param mat_ the material for this model.
     */
    Gmeshmodel(Gmaterial* mat) :
        Gmodel(getDefaultName()),
        linearInterpolation(true),
        mat(mat),
        vData(0),
        oTree(0) {}
    
    Gmodel* clone() const;
    
    // Destructor.
    virtual ~Gmeshmodel()
    {
        freeData();
    }
    
    /**
     * Loads a mesh model from a ply file into this class.
     * @param filename the name of the ply file.
     * @param coeff multiply the 3-D coordonates in the file by this coefficient.
     * @param DX,DY,DZ move the 3-D coordinates in the file by these delta.
     */
    virtual void read_ply ( const std::string& filename, const double& coeff=1.0,
                           const double& DX=0., const double& DY=0., const double& DZ=0. );
    
    /**
     Merge close vertices. Two vertices are merged if the distance between
     them is less than 'eps'.
     */
    virtual void mergeCloseVertices ( double eps = 10e-6 );
    
    /**
     * Refines the model by using the Catmul-Clark subdivision.
     * @param n the number of step to perform.
     */
    virtual void subdivideCatmulClark ( const int& n=1 );
    
    /**
     * Refines the model by using the sqrt3 subdivision.
     * Warning: the models must only use triangles.
     * @param nbStep the number of step to perform.
     */
    virtual void subdivideSqrt3 ( const int& nbStep=1 );
    
    /**
     * Computes normals, and other internal variables, for every vertices.
     */
    virtual void compute_normals();
    
    /**
     * Computes the intersection with a Ray.
     * @param r the Ray.
     * @param t the resulting coefficient of the intersection.
     * @return true if the Ray intercepts this model.
     */
    virtual bool  get_first_intersection ( Ray &r,double & t );
    /**
     * Computes the intersection with a Ray and the normal on that point.
     * @param r the Ray.
     * @param t the resulting coefficient of the intersection.
     * @param n the resulting normal on the intersection point.
     * @return true if the Ray intercepts this model.
     */
    virtual bool get_first_intersection ( Ray &r, double& t, Point& n );
    
    /**
     * Computes solutions of the equation
     *    Ax² + Bx + C = 0
     *
     * @param A coefficient of the term 'x²'.
     * @param B coefficient of the term 'x'.
     * @param C coefficient of the independt term.
     * @param sol1 the first solution of the equation (if any), anything else.
     * @param sol2 the second solution of the equation (if any), anything else.
     * @return the number of solutions, -1 if A, B and C are equal to 0 (any x
     *         is a solution of the equation).
     */
    virtual int solvesEquation ( double A, double B, double C,
                                double& sol1, double& sol2 );
    
    /**
     * Computes solutions of the equation
     *    Bx + C = 0
     * where B is not equal to 0.
     *
     * @param B coefficient of the term 'x'.
     * @param C coefficient of the independt term.
     * @param sol the solution of the equation (if any), anything else.
     */
    virtual void solvesFirstDegreeEquation ( double B, double C, double& sol );
    
    /**
     * Computes solutions of the equation
     *    Ax² + Bx + C = 0
     * where A is not equal to 0.
     *
     * @param A coefficient of the term 'x²'.
     * @param B coefficient of the term 'x'.
     * @param C coefficient of the independt term.
     * @param sol1 the first solution of the equation (if any), anything else.
     * @param sol2 the second solution of the equation (if any), anything else.
     * @return the number of solutions.
     */
    virtual int solvesSecondDegreeEquation ( double A, double B, double C,
                                            double& sol1, double& sol2 );
    
    /**
     * Compute U for a bilinear patch according to V.
     *
     * @param v V value.
     * @param A1 param for bilinear patch.
     * @param B1 param for bilinear patch.
     * @param C1 param for bilinear patch.
     * @param D1 param for bilinear patch.
     * @param A2 param for bilinear patch.
     * @param B2 param for bilinear patch.
     * @param C2 param for bilinear patch.
     * @param D2 param for bilinear patch.
     */
    virtual double computeU ( double v,
                             double A1, double B1, double C1, double D1,
                             double A2, double B2, double C2, double D2 );
    
    /**
     * Compute the T value of the Ray parametric equation (P(t) = r.p + T * r.v)
     * that intersect the face number 'faceNb' at (u, v) position on the
     * bilinear patch defined.
     *
     * @param u U value.
     * @param v V value.
     * @param r the Ray.
     * @param faceNb number of the bilinear patch.
     */
    virtual double computeT ( double u, double v, Ray r, int faceNb );
    
    /**
     * It is by other classes to get the normal on a point.
     * No computation is done here, it just returns the pre-calculated normal.
     * @param p the point.
     * @param pre_n the pre-calculated normal (from get_first_intersection).
     * @param n the normal, will always be 'pre_n'.
     */
    virtual void normal ( const Point& p, const Point& pre_n, Point& n );
    
    /**
     * It is by other classes to get the tangent on a point.
     * No computation is done here, it just returns the pre-calculated tangent.
     * @param p the point.
     * @param pre_t the pre-calculated tangent (from get_first_intersection).
     * @param t the tangent, will always be 'pre_t'.
     */
    virtual void tangent ( const Point& p, const Point& pre_t, Point& t );
    
    /**
     * It is by other classes to get the binormal on a point.
     * No computation is done here, it just returns the pre-calculated binormal.
     * @param p the point.
     * @param pre_bn the pre-calculated binormal (from get_first_intersection).
     * @param bn the binormal, will always be 'pre_bn'.
     */
    virtual void binormal ( const Point& p, const Point& pre_bn, Point& bn );
    
    /**
     * Gets the material of this model.
     */
    virtual Gmaterial* getMaterial()
    {
        return mat;
    }
    
    /**
     * Draws the model using openGL.
     */
    virtual void draw_opengl ( void );
    // Ajouté par le groupe 1
    virtual int typeID()
    {
        return GMESHMODEL;
    }
    std::vector<Point> getVertices()
    {
        return vertices;
    }
    std::vector<int> getVerticeIndexes(Point center, float radius) const;
    
    Point getMin()
    {
        return min;
    }
    Point getMax()
    {
        return max;
    }
    
    // OTree methods
    /*
     Building OTree
     */
    /**
     Build the 'oTree' (used to optimize rendering)
     */
    virtual void buildOTree();
    
    /**
     Add a vertex to the Tree 't' given. 'ttl' is the maximal depth of the
     tree (used when 2 vertices are really close).
     */
    virtual void addVertexToTree ( int ttl, int vID, Tree* t );
    
    /**
     Return True if the point 'p' is inside the sub-box 'boxNb' of the
     main box defined by 'min' and 'max' points.
     */
    virtual bool isInInternalBoxI ( Point p, Point min, Point max,
                                   int boxNb );
    
    /**
     Return True if the point 'p' is inside the box defined by 'min' and 'max'
     points.
     */
    virtual bool isInBox ( Point p, Point min, Point max,
                          double eps = 10e-6 );
    
    /**
     Computes 'min' and 'max' coordinates of sub-box 'boxNb' of the main
     box defined by 'min' and 'max' intot 'iMin' and 'iMax'
     */
    virtual void getInternalBoxIMinMax ( Point min, Point max,
                                        Point& iMin, Point& iMax,
                                        int boxNb );
    
    /*
     Using OTree
     */
    /**
     Store indexes of faces that have to be tested for rendering, according
     to the Ray given.
     */
    virtual void getIntersectedFaces ( int n, Ray r, Tree* box,
                                      std::set<int>& intTri,
                                      std::set<int>& intQuad );
    /**
     True if the box 'box' is intersected by Ray 'r'.
     */
    virtual bool isHitByRay ( Tree* box, Ray r );
    
    /**
     True if the box 'box' is intersected by Ray 'r'.
     'tnear' and 'tfar' ar used to to store 't' values for the first
     and second intersection.
     */
    virtual bool isHitByRay ( Tree* box, Ray r, double& tnear, double& tfar );
    
    /**
     Prints out the tree given.
     */
    virtual void printTree ( int tab, Tree* t );
    
    void getUVFromPoint ( Point point, double& u, double& v, double tilingU, double tilingV );
    
    //////////////////////////////////////////////////////////////////////////////
private:
    
    // Real definitions are at the end of this file.
    struct AdjEdge;
    struct AdjFace;
    struct Tri;
    struct Quad;
    class VertexData;
    
    // The material of this model.
    Gmaterial* mat;
    
    // Points used to define a box wrapping this model.
    Point min;
    Point max;
    
    // The following vectors are used to define vertices and their attributes.
    // index in the vectors = index of the vertex
    std::vector<Point> vertices;  // The 3-D coordinate of the vertex.
    std::vector<Point> normals;   // The normal of the vertex.
    std::vector<AdjEdge*> adjEdges; // The list of edges containing the vertex.
    std::vector<AdjFace*> adjFaces; // The list of faces containing the vertex.
    
    // The following vectors are used to define faces.
    std::vector<Tri>  triFaces;  // triangle
    std::vector<Quad> quadFaces; // quad
    
    // Data storage of CC subdivision. Empty except during CC subdivision.
    VertexData** vData;
    int maxCapacity;
    
    // Coefficients for the sqrt3 subdivision, two by valence.
    std::vector<double> alphas; // = 1 - alpha   (with alpha fct of the valence)
    std::vector<double> betas;  // = alpha / valence
    
    // Tree used to optimization
    Tree* oTree;
    
    /**
     * true if the Ray 'r' is crossing the wrapping box formed by 'min' and 'max'.
     */
    bool boxIntersection ( Ray r, Point min, Point max );
    
    //////////////////////////////////////////////////////////////////////////////
    // Catmul-Clark subdivision.
    
    /*
     1st pass
     - Add Face Vertices by dividing current faces.
     - Add Edge Vertices with incorrect position (which will be rectified
     by the 2nd pass).
     */
    void firstCCPass ( std::vector<int>& edgeVertexLabels,
                      std::vector<AdjEdge*>& newAdjEdges,
                      std::vector<AdjFace*>& newAdjFaces,
                      std::vector<Quad>& newQuadFaces );
    
    /*
     2nd pass
     - Adjust Edge Vertices position.
     */
    void secondCCPass ( std::vector<int>& edgeVertexLabels,
                       std::vector<AdjEdge*>& newAdjEdges,
                       std::vector<AdjFace*>& newAdjFaces );
    
    /*
     3rd pass
     - Adjust Inital Vertices position.
     */
    void thirdCCPass ( int nbIniVert,
                      std::vector<AdjEdge*>& newAdjEdges,
                      std::vector<AdjFace*>& newAdjFaces );
    
    /*
     Adjust the position of the Edge Vertex given, if there is enough
     information to adjust it.
     */
    void refreshEdgeVertex ( int vID,
                            std::vector<AdjEdge*>& adjEdges,
                            std::vector<AdjFace*>& adjFaces );
    
    /*
     Adjust the position of the Initial Vertex given, if there is enough
     information to adjust it.
     */
    void refreshInitialVertex ( int vID,
                               std::vector<AdjEdge*>& adjEdges,
                               std::vector<AdjFace*>& adjFaces );
    
    /*
     Split Edge (a, b) and return the label of the mid-point.
     'splittedEdges' is a 2D arRay.
     If the cell (i, j) is -1, the edge (i, j) has not been splitted.
     Else, the value corresponds to the vertex label that cuts this edge.
     */
    int splitEdge ( int aLabel, int bLabel,
                   std::map< std::pair<int, int>, int >& splittedEdges,
                   std::vector<int>& edgeVertexLabels,
                   std::vector<AdjEdge*>& newAdjEdges,
                   std::vector<AdjFace*>& newAdjFaces );
    
    /*
     Split QuadFace (a, b, c, d) (order is important and QuadFace must exist).
     'splittedEdges' is a 2D arRay.
     If the cell (i, j) is -1, the edge (i, j) has not been splitted.
     Else, the value corresponds to the vertex label that cuts this edge.
     */
    void splitFace ( int aLabel, int bLabel, int cLabel, int dLabel,
                    std::map< std::pair<int, int>, int >& splittedEdges,
                    std::vector<int>& edgeVertexLabels,
                    std::vector<AdjEdge*>& newAdjEdges,
                    std::vector<AdjFace*>& newAdjFaces,
                    std::vector<Quad>& newQuadFaces );
    
    /*
     Split TriFace (a, b, c) (order is important and TriFace must exist).
     'splittedEdges' is a 2D arRay.
     If the cell (i, j) is -1, the edge (i, j) has not been splitted.
     Else, the value corresponds to the vertex label that cuts this edge.
     */
    void splitFace ( int aLabel, int bLabel, int cLabel,
                    std::map< std::pair<int, int>, int >& splittedEdges,
                    std::vector<int>& edgeVertexLabels,
                    std::vector<AdjEdge*>& newAdjEdges,
                    std::vector<AdjFace*>& newAdjFaces,
                    std::vector<Quad>& newQuadFaces );
    
    //////////////////////////////////////////////////////////////////////////////
    // sqrt3 subdivision.
    
    /**
     * Smoothing part of sqrt3.
     * @param index the index of the vertex to smooth.
     * @param step1 true if it is impair step (1,3,5,...).
     * @param oldSize size of the old 'vertices' vector.
     * @param newVertices the new 'vertices' vector.
     */
    void smooth ( const int& index, const bool& step1, const int& oldSize,
                 std::vector<Point>& newVertices );
    
    /**
     * Splitting (and flipping) part of sqrt3.
     * @param index the index of the triangle to split.
     * @param step1 true if it is impair step (1,3,5,...).
     * @param oldSize size of the old 'vertices' vector.
     * @param newVertices the new 'vertices' vector.
     * @param newAdjEdges the new 'adjEdges' vector.
     * @param newAdjFaces the new 'adjFaces' vector.
     * @param newTriFaces the new 'triFaces' vector.
     * @param borders a vector for the border triangles which must be inserted at
     *                the end of the new 'triFaces' vector.
     */
    void split ( const int& index, const bool& step1, const int& oldSize,
                std::vector<Point> &newVertices,
                std::vector<AdjEdge*>& newAdjEdges,
                std::vector<AdjFace*>& newAdjFaces,
                std::vector<Tri>& newTriFaces,
                std::vector<Tri>& borders );
    
    /**
     * Border triangles must be manage in a special way every other step in sqrt3.
     * @param index the index of the triangle to manage.
     * @param oldSize size of the old 'vertices' vector.
     * @param nbBorders the number of border triangles.
     * @param newVertices the new 'vertices' vector.
     * @param newAdjEdges the new 'adjEdges' vector.
     * @param newAdjFaces the new 'adjFaces' vector.
     * @param newTriFaces the new 'triFaces' vector.
     */
    void borderStep2 ( const int& index, const int& oldSize, const int& nbBorders,
                      std::vector<Point>& newVertices,
                      std::vector<AdjEdge*>& newAdjEdges,
                      std::vector<AdjFace*>& newAdjFaces,
                      std::vector<Tri>& newTriFaces );
    
    /**
     * If necessary, initialize the alphas/betas coefficients of sqrt3.
     * @param valence the number of adjacent vertices.
     */
    void initAlphaBeta ( const int& valence );
    
    //////////////////////////////////////////////////////////////////////////////
    // Handling data vectors.
    
    /**
     * Searches a triangle adjacent to an edge.
     * @param a the index of one side of the edge.
     * @param b the index of the other side of the edge.
     * @param currentTriangle the index of the current triangle.
     * @return the index of the first adjacent triangle (but not the current one).
     */
    int searchAdjTriangle ( const int& a, const int& b, const int& currentTriangle );
    
    /**
     * Searches the next border edge.
     * @param a the index of a border vertex.
     * @param b the index of a border vertex adjacent to a.
     * @return the index of a border vertex c such that a-b-c are consecutive.
     */
    int searchNextBorderVertex ( const int& a, const int& b );
    
    /**
     * Adds a triangle to the model.
     * Vertices must be ordered to ensure the right normal computation.
     * @param a the index of the first vertex of the triangle.
     * @param b the index of the second vertex of the triangle.
     * @param c the index of the third vertex of the triangle.
     */
    void addTriangle ( const int& a, const int& b, const int& c );
    
    /**
     * Adds a triangle to the model.
     * Vertices must be ordered to ensure the right normal computation.
     * @param a the index of the first vertex of the triangle.
     * @param b the index of the second vertex of the triangle.
     * @param c the index of the third vertex of the triangle.
     * @param newAdjEdges the 'adjEdges' vector to use.
     * @param newAdjFaces the 'adjFaces' vector to use.
     * @param newTriFaces the 'triFaces' vector to use.
     */
    void addTriangle ( const int& a, const int& b, const int& c,
                      std::vector<AdjEdge*>& newAdjEdges,
                      std::vector<AdjFace*>& newAdjFaces,
                      std::vector<Tri>& newTriFaces );
    
    /**
     * Adds a quad to the model.
     * Vertices must be ordered to ensure the right normal computation.
     * @param a the index of the first vertex of the quad.
     * @param b the index of the second vertex of the quad.
     * @param c the index of the third vertex of the quad.
     * @param d the index of the fourth vertex of the quad.
     */
    void addQuad ( const int& a, const int& b, const int& c, const int& d );
    
    /**
     * Adds a quad to the model.
     * Vertices must be ordered to ensure the right normal computation.
     * @param a the index of the first vertex of the quad.
     * @param b the index of the second vertex of the quad.
     * @param c the index of the third vertex of the quad.
     * @param d the index of the fourth vertex of the quad.
     * @param newAdjEdges the 'adjEdges' vector to use.
     * @param newAdjFaces the 'adjFaces' vector to use.
     * @param newquadFaces the 'quadFaces' vector to use.
     */
    void addQuad ( const int& a, const int& b, const int& c, const int& d,
                  std::vector<AdjEdge*>& newAdjEdges,
                  std::vector<AdjFace*>& newAdjFaces,
                  std::vector<Quad>& newQuadFaces );
    
    /**
     * Adds an edge to the model.
     * @param a the index of one side of the edge.
     * @param b the index of the other side of the edge.
     * @param newAdjEdges the 'adjEdges' vector to use.
     */
    void addEdge ( const int& a, const int& b, std::vector<AdjEdge*>& newAdjEdges );
    
    /*
     * Returns the number of faces adjacent to the edge [a, b].
     */
    int nbAdjFaces ( const int& a, const int& b );
    
    /**
     * Frees pointers from the 'adjEdges' and 'adjFaces' data vectors.
     */
    void freeData();
    
    //////////////////////////////////////////////////////////////////////////////
    
    /**
     * Structure defining a triangle.
     */
    struct Tri
    {
        int vi[3]; // The index of the 3 vertices.
        Point u; // The vector v[0]-v[1]
        Point v; // The vector v[0]-v[2]
        Point n; // The vector normal to this triangle.
        double uu; // The dot prod u * u
        double uv; // The dot prod u * v
        double vv; // The dot prod v * v
        double denum; // uu * vv - uv * uv
        
        Tri() {}
        Tri ( int a, int b, int c )
        {
            vi[0] = a;
            vi[1] = b;
            vi[2] = c;
        }
        
        Tri (Tri * toCopy)
        {
            vi[0] = toCopy->vi[0];
            vi[1] = toCopy->vi[1];
            vi[2] = toCopy->vi[2];
            u = toCopy->u;
            v = toCopy->v;
            n = toCopy->n;
            uu = toCopy->uu;
            uv = toCopy->uv;
            vv = toCopy->vv;
            denum = toCopy->denum;
        }

    };
    
    /**
     * Structure defining a quad.
     */
    struct Quad
    {
        unsigned int vi[4]; // The index of the 4 vertices.
        Point a;
        Point b;
        Point c;
        Point d;
        Point n; // The vector normal to this quad.
        
        Quad() {}
        Quad ( int a, int b, int c, int d )
        {
            vi[0] = a;
            vi[1] = b;
            vi[2] = c;
            vi[3] = d;
        }
        
        Quad (Quad * toCopy)
        {
            vi[0] = toCopy->vi[0];
            vi[1] = toCopy->vi[1];
            vi[2] = toCopy->vi[2];
            vi[3] = toCopy->vi[3];
            a =  Point(toCopy->a);
            b =  Point(toCopy->b);
            c =  Point(toCopy->c);
            d =  Point(toCopy->d);
            n =  Point(toCopy->n);
            
        }

    };
    
    /**
     * Structure defining the chaining list of edges adjacent to a vertex.
     */
    struct AdjEdge
    {
        unsigned int neighbour; // The index of the other side of the edge.
        bool border; // true if this is a border edge.
        struct AdjEdge* next; // The next adjacent edge (0 = no more adjacent edge).
        int size;
        
        AdjEdge ( int a, AdjEdge* n=0, bool b=true )
        {
            neighbour = a;
            border = b;
            next = n;
            size = ( n != 0 ) ? ( n->size + 1 ) : 1;
        }
        
        AdjEdge (const AdjEdge& toCopy)
        {
            neighbour = toCopy.neighbour;
            border = toCopy.border;
            size = toCopy.size;
            
            if(toCopy.next != NULL)
                next = new AdjEdge (*toCopy.next);
            else
                next = NULL;
        }

    };
    
    /**
     * Structure defining the chaining list of faces adjacent to a vertex.
     */
    struct AdjFace
    {
        int nbVertices; // The number of vertices in the face.
        unsigned int index; // The index of the face (in 'triFaces' or 'quadFaces').
        struct AdjFace* next; // The next adjacent face (0 = no more adjacent face).
        int size;
        
        AdjFace ( int i, AdjFace* n=0, int nb=3 )
        {
            nbVertices = nb;
            index = i;
            next = n;
            size = ( n != 0 ) ? ( n->size + 1 ) : 1;
        }
        
        AdjFace( const AdjFace& toCopy)
        {
            nbVertices = toCopy.nbVertices;
            index = toCopy.index;
            size = toCopy.size;
            
            if(toCopy.next != NULL)
                next = new AdjFace(*(toCopy.next));
            else
                next = NULL;
        }

    };
    
    /**
     * Data associated to initial vertices.
     * Set and used by the Catmul-Clark subdivision.
     */
    class VertexData
    {
    public:
        Point vE, vF;
        int     nE, nF;
        int nbIniAdjFaces; // used for edge vertices only
        
        VertexData() : vE ( Point ( 0,0,0 ) ), vF ( Point ( 0,0,0 ) ),
        nE ( 0 ), nF ( 0 ) {};
        
        void addE ( Point p )
        {
            vE = vE + p;
            nE++;
        }
        
        void addF ( Point p )
        {
            vF = vF + p;
            nF++;
        }
    };
    
    /**
     * Tree used for optimization
     */
    class Tree
    {
    public:
        Point min; // Bottom left-left    point of the box
        Point max; // Top    right-right  point of the box
        bool isEmpty; // True if the box contains nothing
        bool oneVertex; // True if a vertex is associated to the box
        std::set<int> vID; // The index value of vertices associated to the box
        
        /*
         Sub-boxes division (8 boxes).
         
         Top:
         |--------|--------|
         |    0   |   1    |
         |--------|--------|
         |    2   |   3    |
         |--------|--------|
         
         Bottom:
         |--------|--------|
         |    4   |   5    |
         |--------|--------|
         |    6   |   7    |
         |--------|--------|
         */
        Tree* childs[8];
        
        Tree() : isEmpty ( true ), oneVertex ( false )
        {
            for ( int i = 0; i < 8; i++ )
                childs[i] = 0;
        }
        
        ~Tree()
        {
            for ( int i = 0; i < 8; i++ )
                if ( childs[i] != 0 ) delete childs[i];
            
            vID.clear();
        }
    };
};

#endif //__MESH_MODEL_H_

// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
