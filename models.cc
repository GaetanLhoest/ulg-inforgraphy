// Gray - A simple ray tracing program
// Copyright (C) 2008-2014 Eric Bechet
//
// See the COPYING file for contributions and license information.
// Please report all bugs and problems to <bechet@cadxfem.org>.
//

#include <cmath>
#include <iostream>
#include <vector>

#include <sstream>
#include <string>

#include <stdlib.h>

#include <stdexcept>
#include <cstdlib>
#ifdef GRAY_HAVE_OGL
#ifdef WIN32
#include "windows.h"
#endif //WIN32
#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif //_APPLE_
#include "glu_gray.h"
#endif
#include "models.h"

#define PI M_PI

unsigned int Gmodel::modelIndex = 0;

string Gmodel::getDefaultName() {

    std::stringstream sstm;
    sstm << "Model" <<  modelIndex ;
    return sstm.str();
}

string Gambient::getDefaultName() {
    return "Ambient" + Gmodel::getDefaultName();
}

void Gambient::transform(const Transform &t)
{

}


string Gdisc::getDefaultName() {
    return "Disc" + Gmodel::getDefaultName();
}

void Gdisc::transform(const Transform &t)
{
    this->P = t.apply(this->P);
    this->n = t.apply(this->n);
}



string Gsphere::getDefaultName() {
    return "Sphere" + Gmodel::getDefaultName();
}

Gmodel* Gsphere::clone() const
{
    return new Gsphere(*this);
}

void Gsphere::transform(const Transform &t)
{
    this->center = t.apply(this->center);
}

bool Gsphere::get_first_intersection ( Ray &ray,double & t )
{
    Point v1 = center-ray.p;
    double tm = v1*ray.v;
    double sqlm = v1*v1-tm*tm;
    double v = r*r-sqlm;

    if ( v<0 ) return false ;

    double dt = sqrt ( v );
    double tplus = tm+dt;
    double tminus = tm-dt;

    if ( tminus >= 0 )
    {
        t = tminus;
        return true;
    }
    else if ( tplus >= 0 )
    {
        t = tplus;
        return true;
    }
    else
    {
        t = 1E100;
        return false;
    }
};

void Gsphere:: draw_opengl ( void )
{
#ifdef GRAY_HAVE_OGL

    //glPushMatrix();

    //glTranslatef(0, 100, 100);

    mat->draw_opengl();

    // position du point le plus haut de la sphere
    double xt=center.X;
    double yt=center.Y+r;
    double zt=center.Z;

    // position du point le plus bas de la sphere
    double xb=center.X;
    double yb=center.Y-r;
    double zb=center.Z;
    int step=5;
    int nptlong=360/step;
    int nptlat=180/step;

    glmeshbuffer buffer ( 1+ ( nptlat-1 ) *nptlong,2* ( nptlat-1 ) *nptlong );

    buffer.add_vertex ( Point ( xt,yt,zt ),Point ( 0,1,0 ) );

    for ( int ph=step; ph<180; ph+=step )
    {
        for ( int th=0; th<360; th+=step )
        {
            buffer.add_vertex ( Point ( center.X+r*cos ( ( th- ( ph-step ) /2 ) *PI/180. ) *sin ( ph*PI/180. ),
                                       center.Y+r*cos ( ph*PI/180. ),
                                       center.Z+r*sin ( ( th- ( ph-step ) /2 ) *PI/180. ) *sin ( ph*PI/180. ) ) ,
                               Point ( cos ( ( th- ( ph-step ) /2 ) *PI/180. ) *sin ( ph*PI/180. ),
                                      cos ( ph*PI/180. ),
                                      sin ( ( th- ( ph-step ) /2 ) *PI/180. ) *sin ( ph*PI/180. ) ) );
        }
    }

    buffer.add_vertex ( Point ( xb,yb,zb ),Point ( 0,-1,0 ) );

    for ( int i=0; i<nptlong; i++ )
    {
        buffer.add_triangle ( 0,i+1, ( i+1 ) %nptlong+1 );
    }

    for ( int j=0; j<nptlat-2; j++ )
    {
        for ( int i=0; i<nptlong; i++ )
        {
            buffer.add_triangle ( j*nptlong+i+1, ( j+1 ) *nptlong+i+1, ( j+1 ) *nptlong+ ( i+1 ) %nptlong+1 );
            buffer.add_triangle ( j*nptlong+i+1, ( j+1 ) *nptlong+ ( i+1 ) %nptlong+1,j*nptlong+ ( i+1 ) %nptlong+1 );
        }
    }

    for ( int i=0; i<nptlong; i++ )
    {
        buffer.add_triangle ( 1+ ( nptlat-1 ) *nptlong,i+1+ ( nptlat-2 ) *nptlong, ( i+1 ) %nptlong+1+ ( nptlat-2 ) *nptlong );
    }

    buffer.draw();

    //glPopMatrix();

#endif// GRAY_HAVE_OGL
}

// calculer la normale au point p d'intersection avec la sphere
void Gsphere::normal ( const Point &p, const Point &pre_n, Point &n )
{
    n = Point ( ( p.X - center.X ) /r, ( p.Y - center.Y ) /r, ( p.Z - center.Z ) /r ) ;
}

void Gsphere::tangent ( const Point& p, const Point& pre_t, Point& t )
{
    Point n;
    normal ( p, n, n );
    t = Point ( -n.Y / n.X, 1, 0 );
    t.normalize();
}

void Gsphere::binormal ( const Point& p, const Point& pre_bn, Point& bn )
{
    Point n;
    normal ( p, n, n );
    Point t;
    tangent ( p, t, t );
    bn = n.crossprod ( t );
}

Gmodel* Gdisc::clone() const
{
    return new Gdisc(*this);
}

bool Gdisc::get_first_intersection ( Ray &ray,double & t )
{
    Point v1=P-ray.p;
    double num=n*v1;
    double denom=n*ray.v;
    t=num/denom;
    Point pos=ray.p+ray.v*t;
    Point diff=P-pos;
    double rr=diff.norm();

    if ( rr<r && t > 0 )
    {
        return true;
    }
    else return false;
}

void  Gdisc::draw_opengl ( void )
{
#ifdef GRAY_HAVE_OGL
    mat->draw_opengl();
    double rr=fabs ( n.XYZ[0] );
    int imin=0;

    for ( int i=1; i<3; ++i )
        if ( fabs ( n.XYZ[i] ) <rr )
        {
            rr=fabs ( n.XYZ[i] );
            i=imin;
        }

    Point v1 ( 0,0,0 );
    v1.XYZ[imin]=1.;
    Point v2=v1.crossprod ( n );
    v2.normalize();
    Point v3=v2.crossprod ( n );
    v3.normalize();
    int step=20;
    //  glColor3f ( getColor().red,getColor().green,getColor().blue);
    glBegin ( GL_TRIANGLE_FAN );
    glVertex3d ( P.X,P.Y,P.Z );

    for ( int i=0; i<=360; i+=step )
    {
        Point p=P+v2*r*sin ( i*PI/180 ) +v3*r*cos ( i*PI/180 );
        glVertex3d ( p.X,p.Y,p.Z );
    }

    glEnd();
#endif// GRAY_HAVE_OGL
}

void Gdisc::normal ( const Point &p, const Point &pre_n, Point &nv )
{
    nv = n;
}

void Gdisc::tangent ( const Point& p, const Point& pre_t, Point& t )
{
    t = n.crossprod ( Point ( 0, 0, 1 ) );
}

void Gdisc::binormal ( const Point& p, const Point& pre_bn, Point& bn )
{
    bn = n.crossprod ( Point ( 1, 0, 0 ) );
}

Gmodel* Gambient::clone() const
{
    return new Gambient(*this);
}

void Gambient::normal ( const Point &p, const Point &pre_n, Point &n )
{
    n=Point ( 0.,0.,0. );
}

void Gambient::tangent ( const Point& p, const Point& pre_t, Point& t )
{
}

void Gambient::binormal ( const Point& p, const Point& pre_bn, Point& bn )
{
}

Glight* Glight::clone() const
{
    return new Glight(*this);
}

void  Glight::draw_opengl ( int i )
{
#ifdef GRAY_HAVE_OGL
    GLfloat light_ambient[] =
    {0.0, 0.0, 0.0, 1.0};
    GLfloat light_diffuse[] =
    {color.red, color.green, color.green, 1.0};
    GLfloat light_specular[] =
    {1.0, 1.0, 1.0, 1.0};
    GLfloat light_position[] =
    {position.X, position.Y, position.Z, 1.0};

    glLightfv ( GL_LIGHT0+i, GL_AMBIENT, light_ambient );
    glLightfv ( GL_LIGHT0+i, GL_DIFFUSE, light_diffuse );
    glLightfv ( GL_LIGHT0+i, GL_SPECULAR, light_specular );
    glLightfv ( GL_LIGHT0+i, GL_POSITION, light_position );
    glEnable ( GL_LIGHT0+i );
#endif// GRAY_HAVE_OGL
}

void Glight::getRay ( Ray &ray )
{
    ray = cache->getRay();
    ray.p = position;
    ray.kfactor = ray.kfactor*color*intensity;
    ray.level = 0;
}


Ray Cache::getRay()
{
    Ray ray;

    do
    {
        ray.v.X = vdcRandomizer.getNext ( 2 ) *2-1;
        ray.v.Y = vdcRandomizer.getNext ( 5 ) *2-1;
        ray.v.Z = vdcRandomizer.getNext ( 7 ) *2-1;
    }
    while ( ray.v*ray.v > 1 );

    ray.v.normalize();

    ray.kfactor = ray.kfactor * 4 * PI;

    return ray;
}

ConicCache::ConicCache ( Point direction_, float cosAperture ) : direction ( direction_ ), cosAperture ( cosAperture )
{
    solidAngle = ( 1-cosAperture ) *2*M_PI;
    sinAperture = sqrtf ( 1-cosAperture*cosAperture );

    direction.normalize();

    Point up ( 0,1,0 );

    if ( up*direction > 0.9 ) up = Point ( 0,0,1 );

    vector1 = direction.crossprod ( up );
    vector1.normalize();
    vector2 = vector1.crossprod ( direction );
    vector2.normalize();
}


Ray ConicCache::getRay()
{
    Ray ray;

    do
    {
        do
        {
            float a,b,c;

            if ( cosAperture < 0 )
            {
                a = rand() * ( 1-cosAperture ) /RAND_MAX+cosAperture;
                b = rand() *2.0/RAND_MAX-1;
                c = rand() *2.0/RAND_MAX-1;
            }
            else
            {
                a = rand() *2.0/RAND_MAX-1;
                b = ( rand() *2.0/RAND_MAX-1 ) *sinAperture;
                c = ( rand() *2.0/RAND_MAX-1 ) *sinAperture;
            }

            ray.v = direction*a + vector1*b + vector2*c;
        }
        while ( ray.v*ray.v > 1 );

        ray.v.normalize();
    }
    while ( ray.v*direction < cosAperture );

    ray.kfactor = Color ( 1,1,1 ) * solidAngle;
    return ray;
}

void Gsphere::getUVFromPoint ( Point point, double& u, double& v, double tilingU, double tilingV )
{
    Point d = center - point;
    d.normalize();

    u = atan2 ( d.Z, d.X ) / ( 2 * PI ) + 0.5;
    v = 0.5 - 2 * asin ( d.Y ) / ( 2 * PI );
}

void Gdisc::getUVFromPoint ( Point point, double& u, double& v, double tilingU, double tilingV )
{
    Point objPoint = point - P;

    u = tilingU * ( objPoint.X / ( 2 * r ) + 0.5 );

    while ( u > 1.0 )
        u -= 1.0;

    v = tilingV * ( objPoint.Z / ( 2 * r ) + 0.5 );

    while ( v > 1.0 )
        v -= 1.0;
}

void Gambient::getUVFromPoint ( Point point, double& u, double& v, double tilingU, double tilingV )
{
}

#undef PI
// kate: indent-mode cstyle; indent-width 2; replace-tabs on;
