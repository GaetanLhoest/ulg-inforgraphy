// Gray - A simple ray tracing program
// Copyright (C) 2008-2014 Eric Bechet
//
// See the COPYING file for contributions and license information.
// Please report all bugs and problems to <bechet@cadxfem.org>.
//

#ifndef __MODELS_H_
#define __MODELS_H_

#include <string>
#include <vector>
#include <cmath>
#include "materials.h"
#include "modelsList.h"
#include "QuasiMonteCarlo.h"
#include "Transform.h"

using namespace std;

class Gmodel
{
protected:

	/**
	 * Index permettant de g�n�rer des noms uniques
	 * (s�quentiels)
	 */
    static unsigned int modelIndex;

    /**
     * Retient le nom du model (identifiant)
     */
    string name;

    /*
     * Retourne un nom par d�faut (unique)
     */
    virtual string getDefaultName();
    
public:
    
    /**
     * Initialise un model nom� 'name'
     */
    Gmodel(string name) : name(name) { modelIndex ++; }
    
    /**
     * Retourne le nom du mod�le
     */
    string getName() const { return name; }
    
    /**
     * Applique une transformation 't' sur le mod�le
     */
    virtual void transform(const Transform &t) = 0;

    /**
     * Retourne une copie du mod�le
     */
    virtual Gmodel* clone() const = 0;

    virtual bool get_first_intersection ( Ray &r,double &t ) =0;
    virtual bool get_first_intersection ( Ray &r,double &t,Point & n )
    {
        return get_first_intersection ( r,t ); // with normal (default implementation)
    }
    virtual void draw_opengl ( void ) =0;
    virtual void normal ( const Point &p, const Point &pre_n, Point &n ) =0;
    virtual void tangent ( const Point& p, const Point& pre_t, Point& t ) =0;
    virtual void binormal ( const Point& p, const Point& pre_bn, Point& bn ) =0;
    virtual Gmaterial* getMaterial() =0;
    virtual ~Gmodel() {}
    virtual int typeID() = 0;
    virtual void getUVFromPoint ( Point point, double& u, double& v, double tilingU, double tilingV ) = 0;
};

class Gsphere : public Gmodel
{
protected:

    virtual string getDefaultName();
    
public:
    
    Gsphere(double r, Point center, Gmaterial* mat) :
        Gmodel(getDefaultName()),
        r(r),
        center(center),
        mat(mat) {}
    
    Gsphere(string name, double r, Point center, Gmaterial* mat) :
        Gmodel(name),
        r(r),
        center(center),
        mat(mat) {}
    
    Gsphere(const Gsphere& s) :
        Gmodel(s.name),
        r(s.r),
        center(s.center),
        mat(s.mat) {}
    
    
    virtual Gmodel* clone() const;
    
    virtual bool get_first_intersection ( Ray &ray,double & t );
    virtual void draw_opengl ( void );
    virtual void normal ( const Point &p, const Point &pre_n, Point &n );
    virtual void tangent ( const Point& p, const Point& pre_t, Point& t );
    virtual void binormal ( const Point& p, const Point& pre_bn, Point& bn );
    virtual Gmaterial* getMaterial()
    {
        return mat;
    }
    virtual int typeID()
    {
        return GSPHERE;
    }
    Point getCenter()
    {
        return center;
    }
    double getRadius()
    {
        return r;
    }
    void getUVFromPoint ( Point point, double& u, double& v, double tilingU, double tilingV );
    
    void transform(const Transform &t);
    
    private :
    double r;
    Point center;
    Gmaterial* mat;
};

class Gambient : public Gmodel
{
protected:
    virtual string getDefaultName();
    
public:
    
    Gambient(Gmaterial* mat) :
        Gmodel(getDefaultName()),
        mat (mat) {}
    
    virtual Gmodel* clone() const;
    
    virtual bool  get_first_intersection ( Ray &ray,double & t )
    {
        t=1e90;
        return true;
    }
    virtual void  draw_opengl ( void ) {}
    virtual void normal ( const Point &p, const Point &pre_n, Point &n );
    virtual void tangent ( const Point& p, const Point& pre_t, Point& t );
    virtual void binormal ( const Point& p, const Point& pre_bn, Point& bn );
    virtual Gmaterial* getMaterial()
    {
        return mat;
    }
    virtual int typeID()
    {
        return GAMBIENT;
    }
    void getUVFromPoint ( Point point, double& u, double& v, double tilingU, double tilingV );
    void transform(const Transform &t);
    private :
    Gmaterial* mat;
};

class Gdisc : public Gmodel
{
protected:
    virtual string getDefaultName();
    
public:
    
    Gdisc(double r,
          Point P,
          Point n,
          Gmaterial* mat) :
        Gmodel(getDefaultName()),
        P(P),
        n(n),
        r (r),
        mat (mat) {}
    
    
    virtual Gmodel* clone() const;
    
    virtual bool  get_first_intersection ( Ray &ray,double & t );
    virtual void  draw_opengl ( void );
    virtual void normal ( const Point &p, const Point &pre_n, Point &n );
    virtual void tangent ( const Point& p, const Point& pre_t, Point& t );
    virtual void binormal ( const Point& p, const Point& pre_bn, Point& bn );
    virtual Gmaterial* getMaterial()
    {
        return mat;
    }
    virtual int typeID()
    {
        return GDISC;
    }
    Point getPoint()
    {
        return P;
    }
    double getRadius()
    {
        return r;
    }
    void getUVFromPoint ( Point point, double& u, double& v, double tilingU, double tilingV );
    void transform(const Transform &t);
    private :
    Point P;
    Point n;
    double r;
    Gmaterial* mat;
};

class Cache
{
    public :
    Cache() : solidAngle ( 4*M_PI ) {}
    virtual ~Cache() {}
    virtual Ray getRay();
    
    protected :
    VdcRandomizer vdcRandomizer;
    float solidAngle;
};

class ConicCache : public Cache
{
    public :
    ConicCache ( Point direction, float cosAperture );
    virtual ~ConicCache() {}
    virtual Ray getRay();
    
    private :
    Point direction;
    float cosAperture;
    float sinAperture;
    
    Point vector1, vector2; //Used when the cone is too small to avoid generating useless vectors
};

class Glight
{
public:
    Glight ( Point p,double i,Color c ) : position ( p ),intensity ( i ),color ( c ),cache ( new Cache() ) {}
    
    virtual Glight* clone() const;
    
    
    Point getPosition()
    {
        return position;
    }
    float getIntensity()
    {
        return intensity;
    }
    Color getColor()
    {
        return color;
    }
    void setCache ( Cache *c )
    {
        delete cache, cache = c;
    }
    void getRay ( Ray &ray );
    virtual void  draw_opengl ( int i=0 );
    
    private :
    Point position;
    float intensity;
    Color color;
    Cache *cache;
};

#endif //__MODELS_H_

// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
