// Gray - A simple ray tracing program
// Copyright (C) 2008-2014 Eric Bechet
//
// See the COPYING file for contributions and license information.
// Please report all bugs and problems to <bechet@cadxfem.org>.
//

#ifndef __NO_OMP_H_
#define __NO_OMP_H_

#ifdef GRAY_HAVE_OMP
#include <omp.h>
#else
static int omp_get_thread_num()
{
  return 0;
}
static int omp_get_num_threads()
{
  return 1;
}
#endif


#endif //__NO_OMP_H_
// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
