// Gray - A simple ray tracing program
// Copyright (C) 2008-2014 Eric Bechet
//
// See the COPYING file for contributions and license information.
// Please report all bugs and problems to <bechet@cadxfem.org>.
//
// Contributed by D. Bourguignon, B. Daene

#include "photonMap.h"
#include "QuasiMonteCarlo.h"
#include "scene.h"
#include "ray.h"
#include <algorithm>
#include "no_omp.h"

template< int dim>
struct Comparer_t
{
  std::vector<Photon> & photons;
  bool operator() ( int i,int j )
  {
    return ( photons[i].pos.XYZ[dim] < photons[j].pos.XYZ[dim] );
  }
};

void KD_tree::balance()
{
  long n = 0;

  while ( 2*n+1 <= photons.size() ) n = 2*n+1;

  long r = photons.size()-n;

  std::vector<int> tab ( photons.size() );

  for ( int i =0; i < photons.size(); i++ )
    tab[i] = i;

  tree.resize ( photons.size() );
  balance ( tab,0,n,r );
  balanced = true;
}

void KD_tree::balance ( std::vector<int> &tab, long node, long n, long r )
{
  if ( n == 0 ) return; // Rien à équilibrer, tab.size() == 0

  /*
  // Calcul de la variance selon les 3 dimensions.
  double sumX = 0, sumY = 0, sumZ = 0;
  double sumX2 = 0, sumY2 = 0, sumZ2 = 0;

  for(int i = 0; i < tab.size(); i++)
  {
    sumX += photons[tab[i]].pos.X; sumX2 += photons[tab[i]].pos.X * photons[tab[i]].pos.X;
    sumY += photons[tab[i]].pos.Y; sumY2 += photons[tab[i]].pos.Y * photons[tab[i]].pos.Y;
    sumZ += photons[tab[i]].pos.Z; sumZ2 += photons[tab[i]].pos.Z * photons[tab[i]].pos.Z;
  }

  double varX = (sumX2 - sumX*sumX/tab.size());///tab.size();
  double varY = (sumY2 - sumY*sumY/tab.size());///tab.size();
  double varZ = (sumZ2 - sumZ*sumZ/tab.size());///tab.size();
  */

  // Calcul de la distance maximum selon les 3 dimensions
  double minX = photons[tab[0]].pos.X, minY = photons[tab[0]].pos.Y, minZ = photons[tab[0]].pos.Z;
  double maxX = photons[tab[0]].pos.X, maxY = photons[tab[0]].pos.Y, maxZ = photons[tab[0]].pos.Z;

  for ( int i = 1; i < tab.size(); i++ )
  {
    if ( photons[tab[i]].pos.X < minX ) minX = photons[tab[i]].pos.X;

    if ( photons[tab[i]].pos.Y < minY ) minY = photons[tab[i]].pos.Y;

    if ( photons[tab[i]].pos.Z < minZ ) minZ = photons[tab[i]].pos.Z;

    if ( photons[tab[i]].pos.X > maxX ) maxX = photons[tab[i]].pos.X;

    if ( photons[tab[i]].pos.Y > maxY ) maxY = photons[tab[i]].pos.Y;

    if ( photons[tab[i]].pos.Z > maxZ ) maxZ = photons[tab[i]].pos.Z;
  }

  double varX = maxX-minX, varY = maxY-minY, varZ = maxZ-minZ;


  if ( varX >= varY && varX >= varZ )
  {
    //séparation selon X
    struct Comparer_t<DIM_X> comparer = {photons};
    sort ( tab.begin(),tab.end(),comparer );
    tree[node].plane = DIM_X;
  }
  else if ( varY >= varX && varY >= varZ )
  {
    //séparation selon Y
    struct Comparer_t<DIM_Y> comparer = {photons};
    sort ( tab.begin(),tab.end(),comparer );
    tree[node].plane = DIM_Y;
  }
  else
  {
    //séparation selon Z
    struct Comparer_t<DIM_Z> comparer = {photons};
    sort ( tab.begin(),tab.end(),comparer );
    tree[node].plane = DIM_Z;
  }

  long i,ng,nd,rg,rd;

  if ( r < ( n+1 ) /2 )
  {
    i = ( n-1 ) /2 + r;
    ng = ( n-1 ) /2;
    nd = ( n-1 ) /2;
    rg = r;
    rd = 0;
  }
  else
  {
    i = n;
    ng = n;
    nd = ( n-1 ) /2;
    rg = 0;
    rd = r - ( n+1 ) /2;
  }

  tree[node].photon = tab[i];

  std::vector<int> tabLess, tabMore;

  for ( int j = 0; j < i; j++ )
    tabLess.push_back ( tab[j] );

  for ( int j = i+1; j < tab.size(); j++ )
    tabMore.push_back ( tab[j] );

  balance ( tabLess, node*2+1, ng, rg );
  balance ( tabMore, node*2+2, nd, rd );
}

std::vector<Photon> KD_tree::getKNearest ( Point &position, Point &normale, int K, float &maxDist2, float minCos )
{
  #pragma omp critical
  {
    if ( !balanced ) balance();
  }

  std::vector<KD_pair> result;

  getKNearest ( position, normale, K, minCos, maxDist2, 0, result );

  std::vector<Photon> nearestPhotons;
  nearestPhotons.reserve ( result.size() );

  for ( int i=0; i<result.size(); i++ )
    nearestPhotons.push_back ( photons[result[i].index] );

  return nearestPhotons;
}

void KD_tree::getKNearest ( Point &position, Point &normale, int K, float minCos, float &maxDist2, long node, std::vector<KD_pair> &result )
{
  // Parcours en profondeur d'abord, on élague les branches trop éloignées.
  if ( node < tree.size() )
  {
    float dist =  position.XYZ[tree[node].plane] - photons[tree[node].photon].pos.XYZ[tree[node].plane];

    if ( dist*dist <= maxDist2 )
    {
      addPhoton ( tree[node].photon, position, normale, K, minCos, maxDist2, result );
      getKNearest ( position, normale, K, minCos, maxDist2, 2*node+1, result );
      getKNearest ( position, normale, K, minCos, maxDist2, 2*node+2, result );
    }
    else
    {
      if ( dist < 0 )
        getKNearest ( position, normale, K, minCos, maxDist2, 2*node+1, result );
      else
        getKNearest ( position, normale, K, minCos, maxDist2, 2*node+2, result );
    }
  }
}

void KD_tree::addPhoton ( int photon, Point &position, Point &normale, int K, float minCos, float &maxDist2, std::vector<KD_pair> &result )
{
  Point delta = photons[photon].pos - position;
  float dist2 = delta*delta;

  if ( dist2 < maxDist2 && photons[photon].nor * normale >= minCos )
  {
    // Result is a heap, top is max.
    // node i has children 2*i+1 and 2*i+2
    if ( result.size() < K )
    {
      //add back and reorder
      result.push_back ( KD_pair ( photon,dist2 ) );

      int i = result.size()-1;

      while ( i > 0 && result[i].dist2 > result[ ( i-1 ) /2].dist2 )
      {
        KD_pair tmp = result[i];
        result[i] = result[ ( i-1 ) /2];
        result[ ( i-1 ) /2] = tmp;

        i = ( i-1 ) /2;
      }

      if ( result.size() == K ) maxDist2 = result[0].dist2;
    }
    else
    {
      //replace front and reorder
      result[0].index = photon;
      result[0].dist2 = dist2;

      int i = 0;

      while ( ( i*2+1 < result.size() && result[i].dist2 < result[i*2+1].dist2 )
              || ( i*2+2 < result.size() && result[i].dist2 < result[i*2+2].dist2 ) )
      {
        if ( i*2+2 < result.size() && result[i*2+1].dist2 < result[i*2+2].dist2 )
        {
          KD_pair tmp = result[i*2+2];
          result[i*2+2] = result[i];
          result[i] = tmp;

          i = i*2+2;
        }
        else
        {
          KD_pair tmp = result[i*2+1];
          result[i*2+1] = result[i];
          result[i] = tmp;

          i = i*2+1;
        }
      }

      maxDist2 = result[0].dist2;
    }
  }
}


unsigned long PhotonMap::populate ( Gscene &scene, unsigned long nbRayPerLight )
{
  VdcRandomizer random;
  unsigned long TotalPhoton = 0;

  for ( Gscene::iteratorLight it = scene.beginLight(); it != scene.endLight(); ++it )
  {
    int counter = 0;

    if ( omp_get_thread_num() == 0 )
    {
      std::cout << "|__________________________________________________|" << std::endl;
      std::cout << "|";
      std::cout.flush();
    }

    Glight *light = *it;
    #pragma omp parallel for reduction(+:TotalPhoton) schedule(dynamic,10)

    for ( int i = 0; i < nbRayPerLight; i++ )
    {
      if ( omp_get_thread_num() == 0 && ( i*50 ) /nbRayPerLight >= counter )
      {
        std::cout << "=";
        std::cout.flush();
        counter++;
      }

      Ray ray;
      light->getRay ( ray );
      TotalPhoton += addRay ( ray,scene );
    }

    std::cout << "|" << std::endl;
  }

  nbRayPerLightTotal += nbRayPerLight;

  lightCoef = 2*typicalLightDist*typicalLightDist/nbRayPerLightTotal;
  std::cout << "Balancing photon map." << std::endl;
  map.balance();

  return TotalPhoton;
}

int PhotonMap::addRay ( Ray &ray, Gscene &scene )
{
  int nbPhotons = 0;

  while ( ray.level < scene.getMaxRecurLevel() && !ray.kfactor.isBlack ( 1E-6 ) )
  {
    HitInfo hitInfo;

    if ( !scene.getFirstIntersection ( ray, hitInfo ) ) break;

    if ( hitInfo.object->getMaterial()->isDiffusive() )
    {
      Photon photon ( hitInfo.point, hitInfo.ray.v, hitInfo.normal, hitInfo.ray.kfactor );
      #pragma omp critical
      {
        map.addPhoton ( photon );
      }
      nbPhotons++;
    }

    if ( !hitInfo.object->getMaterial()->getNextRay ( hitInfo,ray,scene ) ) break;
  }

  return nbPhotons;
}

void PhotonMap::shade ( Color & c, HitInfo & hitInfo )
{
  if ( !hitInfo.object->getMaterial()->isDiffusive() && !hitInfo.object->getMaterial()->isReflective() )
    return;

  Color k ( 0,0,0 );
  float maxDist2 = spotRadius2;

  std::vector<Photon> photons = map.getKNearest ( hitInfo.point, hitInfo.normal, nbPhotonsPerSpot, maxDist2 );

  for ( int i=0; i < photons.size(); i++ )
  {
    Color factor;
    hitInfo.object->getMaterial()->brdf ( photons[i].dir, hitInfo.normal, hitInfo.ray.v, factor );

    Point delta = photons[i].pos - hitInfo.point;
    float cosI = - ( photons[i].dir * hitInfo.normal );
    float dist2 = delta*delta;
    k = k + photons[i].color*factor* ( ( 1-dist2/maxDist2 ) /maxDist2*lightCoef ) /cosI;
  }

  c = c + k;
}
// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
