// Gray - A simple ray tracing program
// Copyright (C) 2008-2014 Eric Bechet
//
// See the COPYING file for contributions and license information.
// Please report all bugs and problems to <bechet@cadxfem.org>.
//


#include <cmath>

#include "ray.h"

#define CLIPCOLOR(r,g,b) Color((r> 1.0)? (1.0):(float)r,(g> 1.0)? (1.0):(float)g,(b> 1.0)? (1.0): (float)b)

Point Point::operator + ( const Point& other ) const
{
  return Point ( X+other.X,Y+other.Y,Z+other.Z );
}

Point Point::operator - ( const Point& other ) const
{
  return Point ( X-other.X,Y-other.Y,Z-other.Z );
}

Point Point::operator * ( double v ) const
{
  return Point ( X*v,Y*v,Z*v );
}

double Point::operator * ( const Point& other ) const
{
  return ( X*other.X+Y*other.Y+Z*other.Z );
}

double Point::norm() const
{
  double r=sqrt ( X*X+Y*Y+Z*Z );
  return r;
}

double Point::normalize ( void )
{
  double r=sqrt ( X*X+Y*Y+Z*Z );

  if ( r > 0.0 )
  {
    X=X/r;
    Y=Y/r;
    Z=Z/r;
  }

  return r;
}

Point Point::crossprod ( const Point& other ) const
{
  return Point ( Y*other.Z-Z*other.Y,Z*other.X-X*other.Z,X*other.Y-Y*other.X );
}

double Point::dotprod ( const Point& other ) const
{
  return ( X*other.X+Y*other.Y+Z*other.Z );
}

const Point operator * ( const Point &p,double v )
{
  return Point ( p.X*v,p.Y*v,p.Z*v );
}

std::ostream& operator<< ( std::ostream& out, const Point& p )
{
  return out << "(" << p.X << ", " << p.Y << ", " << p.Z << ")";
}

Color Color::operator + ( const Color& other ) const
{
  return CLIPCOLOR ( red+other.red,green+other.green,blue+other.blue );
}

Color Color::operator * ( const Color& other ) const
{
  return CLIPCOLOR ( red*other.red,green*other.green,blue*other.blue );
}

Color Color::operator * ( double v ) const
{
  return CLIPCOLOR ( red*v,green*v,blue*v );
}

Color Color::operator / ( double v ) const
{
  return CLIPCOLOR ( red/v,green/v,blue/v );
}
// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
