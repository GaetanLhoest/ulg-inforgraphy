// Gray - A simple ray tracing program
// Copyright (C) 2008-2014 Eric Bechet
//
// See the COPYING file for contributions and license information.
// Please report all bugs and problems to <bechet@cadxfem.org>.
//

#ifndef __Ray_H_
#define __Ray_H_

#include <iostream>

class Point
{
public:
  Point ( void ) {}
  Point ( double x,double y,double z ) : X ( x ),Y ( y ),Z ( z ) {}
  Point ( const Point & other )
  {
    *this=other;
  }
  Point operator + ( const Point& other ) const ;
  Point operator - ( const Point& other ) const ;
  Point operator * ( double v ) const ;
  double operator * ( const Point& other ) const ;
  double norm() const ;
  double normalize ( void ) ;
  Point crossprod ( const Point& other ) const ;
  double dotprod ( const Point& other ) const;
  union
  {
    double XYZ[3];
    struct
    {
      double X,Y,Z;
    };
  };

  friend std::ostream& operator<< ( std::ostream& out, const Point& p );
};

const Point operator * ( double v,const Point &p );

#define BL 1E-3 // colors below this intensity are considered black
class Color
{
public :
  Color() {}
//    Color ( unsigned char R,unsigned char G, unsigned char B ) {red=R/255.;green=G/255.;blue=B/255.;}
  Color ( float R,float G, float B )
  {
    red=R;
    green=G;
    blue=B;
  }
  union
  {
    float RGB[3];
    struct
    {
      float red,green,blue;
    };
  };
  Color operator + ( const Color& other ) const ;
  Color operator * ( const Color& other ) const ;
  Color operator * ( double v ) const;
  Color operator / ( double v ) const;
  bool isBlack ( float black = BL ) const
  {
    return red+green+blue < black;
  }
};

class Ray
{
public:
  Ray() : level ( 0 ) {}
  Ray ( Point p_, Point v_, Color kfactor = Color ( 1,1,1 ), int l_ = 0 ) : p ( p_ ), v ( v_ ), kfactor ( kfactor ), level ( l_ ) {};

  // p : Ray start
  // v : Ray direction
  Point p,v;
  int level; // number of reflexions/refractions (starts at 0)
  Color kfactor; // color factor from reflexions/refractions.
};

#endif //__Ray_H_


// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
