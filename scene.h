// Gray - A simple ray tracing program
// Copyright (C) 2008-2014 Eric Bechet
//
// See the COPYING file for contributions and license information.
// Please report all bugs and problems to <bechet@cadxfem.org>.
//

#ifndef __SCENE_H_
#define __SCENE_H_

#include "models.h"
#include "camera.h"
#include "BVTree.h"
#include "photonMap.h"
#include <list>
#include <map>
#include <string>
#include "Group.h"

class Group;

using namespace std;

/*
 * HitInfo stores actual hit information for each hit
 */
class HitInfo
{
public:
    Gmodel* object;  //  the object hit
    Point point;    // hit point
    Point normal;  // normal at hit point
    float distance;
    Ray ray;
};



class Gscene
{
public:

    typedef std::map<std::string, Gmodel*> ModelMap;
    typedef ModelMap::iterator iteratorModel;
    
    typedef std::map<std::string, Group*> GroupMap;
    typedef GroupMap::iterator iteratorGroup;
    
    typedef std::list<Glight*>::iterator iteratorLight;

private:
    ModelMap            modlist;
    GroupMap            groupMap;
    std::list<Glight*>  lightlist;
    Point               viewpoint;
    Camera*             camera;
    int maxRecurLevel;
    int Oversample;
    unsigned int BlurLevel;
    // Ajouté par le groupe 1
    BVTree* tree;
    int counter;
#ifdef GRAY_HAVE_PHOTONMAP
    PhotonMap photonMap;
#endif
    float refractiveIndex;
    float typicalLightDist;
    bool photonLighting;
    bool directLighting;

public:

    Gscene() :
        maxRecurLevel(5),
        Oversample(1),
        BlurLevel(1),
        tree(NULL),
        counter(0),
        #ifdef GRAY_HAVE_PHOTONMAP
            photonMap(10, 1000000, 282),
        #endif
        refractiveIndex(1),
        typicalLightDist(282),
        photonLighting(true),
        directLighting(true)
        { }

    Gscene(Gscene& scene);

    ~Gscene();

    int getMaxRecurLevel() { return maxRecurLevel; }
    void setMaxRecurLevel(int l) { maxRecurLevel = l; }
    
    int  getOversampling() { return Oversample;}
    void setOversampling(int l) { Oversample = l; }
    

    Camera* getCam() { return camera; }
    void setCam (Camera* c) { camera = c; }
    
    unsigned int getBlurLevel() { return BlurLevel; }
    void setBlurLevel(int b) { BlurLevel = std::max(b, 1); }

    void setTypicalLightDist(float dist) { typicalLightDist = dist; }
    void setDirectLighting (bool value) { directLighting = value; }
#ifdef GRAY_HAVE_PHOTONMAP
    void setPhotonLighting (bool value) { photonLighting = value; }
#endif
    
    void insertModel(Gmodel* m);
    Gmodel * getModel(string name) const;
    
    void insertGroup(Group* g);
    Group * getGroup(string name) const;
    
    void insertLight ( Glight* l ) ;
    
    iteratorModel beginModel() ;
    iteratorModel endModel();
    
    iteratorGroup beginGroup();
    iteratorGroup endGroup();
    
    iteratorLight beginLight() ;
    iteratorLight endLight();
    
    void setViewpoint ( Point v ) ;
    Point getViewpoint() ;
    
    void shade ( Color&     k, HitInfo &hitInfo );
    bool isInShadow ( Ray& r, Gmodel* m, double d );
    void raytracing ( Ray & r, Color & c );
    bool getFirstIntersection ( Ray &r, HitInfo &hitInfo );

    int getCounter() { return counter; }
    void initCounter() { counter = 0; }
    
    void initBVTree() { tree = new BVTree ( this ); }
    BVTree* getTree() { return tree; }
    
#ifdef GRAY_HAVE_PHOTONMAP
    unsigned long initPhotonMap ( unsigned long nbRayPerLight, float spotRadius, long nbPhotonsPerSpot )
    {
        photonMap = PhotonMap ( spotRadius, nbPhotonsPerSpot, typicalLightDist );
        return photonMap.populate ( *this, nbRayPerLight );
    }
#endif
    
    void setRefractiveIndex ( float n ) { refractiveIndex = n; }
    float getRefractiveIndex() { return refractiveIndex; }
};




#endif //__SCENE_H_
// kate: indent-mode cstyle; indent-width 2; replace-tabs on;
