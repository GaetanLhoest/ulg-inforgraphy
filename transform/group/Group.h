//
//  Group.h
//  GRAY
//
//  Created by Sébastien Rigaux on 15/04/14.
//
//

#ifndef __GRAY__Group__
#define __GRAY__Group__

#include <iostream>
#include <set>
#include <string>
#include <sstream>

#include "ray.h"
#include "models.h"
#include "scene.h"
class Gscene;

using namespace std;

typedef std::set<string> StringSet;

class Group {
    
protected:
    string name;
    StringSet subGroupsNames;
    
    Point transformOrigin;
    
    static unsigned int groupIndex;
    
    virtual string getDefaultName() const;
    virtual void apply(const Gscene &scene, Transform t) = 0;
    
    void addSubGroup(const Group& group);
    void add(const Group& group) { addSubGroup(group); }
    
    Group(string name, Point transformOrigin) :
        name(name),
        subGroupsNames(),
        transformOrigin(transformOrigin)
        { groupIndex ++; }
    
public:
    
    Group():
        name(getDefaultName()),
        subGroupsNames(),
        transformOrigin(Point(0,0,0))
        {}
    
    Group(const Group& g):
        name(g.name),
        subGroupsNames(g.subGroupsNames),
        transformOrigin(g.transformOrigin)
        {}
    
    virtual Group* clone() const = 0;
    
    string getName() const { return this->name; }
    void setName(const string name) { this->name = name; }
    
    
    StringSet getSubGroupsNames() const { return subGroupsNames; }
    
    
    Point getTransformOrigin() const { return transformOrigin; }
    void setTransformOrigin(Point p) { transformOrigin = p;}
    
    void apply(const Gscene &scene, double fraction, Transform transform);

};


#endif /* defined(__GRAY__Group__) */
